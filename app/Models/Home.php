<?php namespace App\Models;



class Home extends Base
{

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'homes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type','title',
        'page_id',
        'category_id',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates  = [];

    protected $presenter = \App\Presenters\HomePresenter::class;

    public static function boot()
    {
        parent::boot();
        parent::observe(new \App\Observers\HomeObserver);
    }

    // Relations
    public function page()
    {
        return $this->belongsTo(\App\Models\Page::class, 'page_id', 'id');
    }

    public function category()
    {
        return $this->belongsTo(\App\Models\Category::class, 'category_id', 'id');
    }

    

    // Utility Functions

    /*
     * API Presentation
     */
    public function toAPIArray()
    {
        return [
            'id' => $this->id,
            'type' => $this->type,
            'page_id' => $this->page_id,
            'category_id' => $this->category_id,
        ];
    }

}
