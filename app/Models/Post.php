<?php namespace App\Models;



class Post extends Base
{

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'posts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','category_id',
        'cover_image_id',
        'description',
        'is_publish',
        'content',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates  = [];

    protected $presenter = \App\Presenters\PostPresenter::class;

    public static function boot()
    {
        parent::boot();
        parent::observe(new \App\Observers\PostObserver);
    }

    // Relations
    public function menu()
    {
        return $this->belongsTo(\App\Models\Menu::class, 'menu_id', 'id');
    }

    public function category()
    {
        return $this->belongsTo(\App\Models\Category::class, 'category_id', 'id');
    }

    public function coverImage()
    {
        return $this->hasOne(\App\Models\Image::class, 'id', 'cover_image_id');
    }

    

    // Utility Functions

    /*
     * API Presentation
     */
    public function toAPIArray()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'menu_id' => $this->menu_id,
            'parent_id' => $this->parent_id,
            'cover_image_id' => $this->cover_image_id,
            'description' => $this->description,
            'is_publish' => $this->is_publish,
            'content' => $this->content,
        ];
    }

}
