<?php namespace App\Models;



class Page extends Base
{

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'cover_image_id',
        'description',
        'is_publish',
        'content','parent_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates  = [];

    protected $presenter = \App\Presenters\PagePresenter::class;

    public static function boot()
    {
        parent::boot();
        parent::observe(new \App\Observers\PageObserver);
    }

    // Relations
    public function menu()
    {
        return $this->belongsTo(\App\Models\Menu::class, 'menu_id', 'id');
    }

    public function coverImage()
    {
        return $this->hasOne(\App\Models\Image::class, 'id', 'cover_image_id');
    }
    public function children()
    {
        return $this->hasMany(\App\Models\Page::class, 'parent_id', 'id');
    }
    public function parent()
    {
        return $this->belongsTo(\App\Models\Page::class, 'parent_id');
    }

    

    // Utility Functions

    /*
     * API Presentation
     */
    public function toAPIArray()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'menu_id' => $this->menu_id,
            'cover_image_id' => $this->cover_image_id,
            'description' => $this->description,
            'is_publish' => $this->is_publish,
            'content' => $this->content,
        ];
    }

}
