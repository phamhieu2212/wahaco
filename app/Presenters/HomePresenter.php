<?php

namespace App\Presenters;

use Illuminate\Support\Facades\Redis;
use App\Models\Page;
use App\Models\Category;

class HomePresenter extends BasePresenter
{
    protected $multilingualFields = [];

    protected $imageFields = [];

    /**
    * @return \App\Models\Page
    * */
    public function page()
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel('PageModel');
            $cached = Redis::hget($cacheKey, $this->entity->page_id);

            if( $cached ) {
                $page = new Page(json_decode($cached, true));
                $page['id'] = json_decode($cached, true)['id'];
                return $page;
            } else {
                $page = $this->entity->page;
                Redis::hsetnx($cacheKey, $this->entity->page_id, $page);
                return $page;
            }
        }

        $page = $this->entity->page;
        return $page;
    }

    /**
    * @return \App\Models\Category
    * */
    public function category()
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel('CategoryModel');
            $cached = Redis::hget($cacheKey, $this->entity->category_id);

            if( $cached ) {
                $category = new Category(json_decode($cached, true));
                $category['id'] = json_decode($cached, true)['id'];
                return $category;
            } else {
                $category = $this->entity->category;
                Redis::hsetnx($cacheKey, $this->entity->category_id, $category);
                return $category;
            }
        }

        $category = $this->entity->category;
        return $category;
    }

    
}
