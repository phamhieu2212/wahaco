<?php

namespace App\Presenters;

use Illuminate\Support\Facades\Redis;
use App\Models\Menu;
use App\Models\Image;

class PostPresenter extends BasePresenter
{
    protected $multilingualFields = [];

    protected $imageFields = ['cover_image'];

    /**
    * @return \App\Models\Menu
    * */
    public function menu()
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel('MenuModel');
            $cached = Redis::hget($cacheKey, $this->entity->menu_id);

            if( $cached ) {
                $menu = new Menu(json_decode($cached, true));
                $menu['id'] = json_decode($cached, true)['id'];
                return $menu;
            } else {
                $menu = $this->entity->menu;
                Redis::hsetnx($cacheKey, $this->entity->menu_id, $menu);
                return $menu;
            }
        }

        $menu = $this->entity->menu;
        return $menu;
    }

    /**
    * @return \App\Models\Parent
    * */

    /**
    * @return \App\Models\Image
    * */
    public function coverImage()
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel('ImageModel');
            $cached = Redis::hget($cacheKey, $this->entity->cover_image_id);

            if( $cached ) {
                $image = new Image(json_decode($cached, true));
                $image['id'] = json_decode($cached, true)['id'];
                return $image;
            } else {
                $image = $this->entity->coverImage;
                Redis::hsetnx($cacheKey, $this->entity->cover_image_id, $image);
                return $image;
            }
        }

        $image = $this->entity->coverImage;
        return $image;
    }

    
}
