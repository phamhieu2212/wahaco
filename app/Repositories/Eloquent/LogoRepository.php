<?php namespace App\Repositories\Eloquent;

use \App\Repositories\LogoRepositoryInterface;
use \App\Models\Logo;

class LogoRepository extends SingleKeyModelRepository implements LogoRepositoryInterface
{

    public function getBlankModel()
    {
        return new Logo();
    }

    public function rules()
    {
        return [
        ];
    }

    public function messages()
    {
        return [
        ];
    }

}
