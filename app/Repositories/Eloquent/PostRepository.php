<?php namespace App\Repositories\Eloquent;

use \App\Repositories\PostRepositoryInterface;
use \App\Models\Post;

class PostRepository extends SingleKeyModelRepository implements PostRepositoryInterface
{

    public function getBlankModel()
    {
        return new Post();
    }

    public function rules()
    {
        return [
        ];
    }

    public function messages()
    {
        return [
        ];
    }

}
