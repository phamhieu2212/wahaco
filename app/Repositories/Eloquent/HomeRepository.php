<?php namespace App\Repositories\Eloquent;

use \App\Repositories\HomeRepositoryInterface;
use \App\Models\Home;

class HomeRepository extends SingleKeyModelRepository implements HomeRepositoryInterface
{

    public function getBlankModel()
    {
        return new Home();
    }

    public function rules()
    {
        return [
        ];
    }

    public function messages()
    {
        return [
        ];
    }

}
