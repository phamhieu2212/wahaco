<?php namespace App\Repositories\Eloquent;

use \App\Repositories\LibraryRepositoryInterface;
use \App\Models\Library;

class LibraryRepository extends SingleKeyModelRepository implements LibraryRepositoryInterface
{

    public function getBlankModel()
    {
        return new Library();
    }

    public function rules()
    {
        return [
        ];
    }

    public function messages()
    {
        return [
        ];
    }

}
