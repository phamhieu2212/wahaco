<?php namespace App\Repositories\Eloquent;

use \App\Repositories\MenuRepositoryInterface;
use \App\Models\Menu;

class MenuRepository extends SingleKeyModelRepository implements MenuRepositoryInterface
{

    public function getBlankModel()
    {
        return new Menu();
    }

    public function rules()
    {
        return [
        ];
    }

    public function messages()
    {
        return [
        ];
    }

}
