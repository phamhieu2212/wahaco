<?php
namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Repositories\CategoryRepositoryInterface;
use App\Http\Requests\BaseRequest;
use App\Repositories\ImageRepositoryInterface;
use App\Repositories\PostRepositoryInterface;
use App\Http\Requests\Admin\PostRequest;
use App\Http\Requests\PaginationRequest;
use App\Services\FileUploadServiceInterface;
use App\Services\PostServiceInterface;

class PostController extends Controller
{
    /** @var  \App\Repositories\PostRepositoryInterface */
    protected $postRepository;
    protected $categoryRepository;
    protected $fileUploadService;
    protected $postService;
    protected $imageRepository;

    public function __construct(
        PostRepositoryInterface $postRepository,
        CategoryRepositoryInterface $categoryRepository,
        FileUploadServiceInterface $fileUploadService,
        PostServiceInterface $postService,
        ImageRepositoryInterface $imageRepository
    ) {
        $this->postRepository = $postRepository;
        $this->categoryRepository = $categoryRepository;
        $this->fileUploadService  = $fileUploadService;
        $this->postService        = $postService;
        $this->imageRepository    = $imageRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param    \App\Http\Requests\PaginationRequest $request
     * @return  \Response
     */
    public function index(PaginationRequest $request)
    {
        $paginate['limit']      = $request->limit();
        $paginate['offset']     = $request->offset();
        $paginate['order']      = $request->order();
        $paginate['direction']  = $request->direction();
        $paginate['baseUrl']    = action('Admin\PostController@index');

        $filter = [];
        $keyword = $request->get('keyword');
        if (!empty($keyword)) {
            $filter['query'] = $keyword;
        }

        $count = $this->postRepository->countByFilter($filter);
        $posts = $this->postRepository->getByFilter($filter, $paginate['order'], $paginate['direction'], $paginate['offset'], $paginate['limit']);

        return view(
            'pages.admin.' . config('view.admin') . '.posts.index',
            [
                'posts'    => $posts,
                'count'         => $count,
                'paginate'      => $paginate,
                'keyword'       => $keyword
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Response
     */
    public function create()
    {
        return view(
            'pages.admin.' . config('view.admin') . '.posts.edit',
            [
                'isNew'     => true,
                'post' => $this->postRepository->getBlankModel(),
                'categories' => $this->categoryRepository->all()
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    $request
     * @return  \Response
     */
    public function store(PostRequest $request)
    {
        $input = $request->only(
            [
                            'name',
                            'category_id',
                            'cover_image_id',
                            'description',
                            'is_publish',
                            'content',
                        ]
        );

        $input['is_enabled'] = $request->get('is_enabled', 0);
        $post = $this->postRepository->create($input);

        if( empty($post) ) {
            return redirect()->back()->with('message-error', trans('admin.errors.general.save_failed'));
        }
        if ($request->hasFile('cover_image_id')) {
            $file = $request->file('cover_image_id');

            $image = $this->fileUploadService->upload(
                'post_image',
                $file,
                [
                    'entity_type' => 'post_image',
                    'entity_id' => $post->id,
                    'title' => $request->input('name', ''),
                ]
            );

            if (!empty($image)) {
                $this->postRepository->update($post, ['cover_image_id' => $image->id]);

            }
        }

        return redirect()->action('Admin\PostController@index')
            ->with('message-success', trans('admin.messages.general.create_success'));
    }

    /**
     * Display the specified resource.
     *
     * @param    int $id
     * @return  \Response
     */
    public function show($id)
    {
        $post = $this->postRepository->find($id);
        if( empty($post) ) {
            abort(404);
        }

        return view(
            'pages.admin.' . config('view.admin') . '.posts.edit',
            [
                'isNew' => false,
                'post' => $post,

                'categories' => $this->categoryRepository->all()
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param    int $id
     * @return  \Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    int $id
     * @param            $request
     * @return  \Response
     */
    public function update($id, PostRequest $request)
    {
        /** @var  \App\Models\Post $post */
        $post = $this->postRepository->find($id);
        if( empty($post) ) {
            abort(404);
        }

        $input = $request->only(
            [
                            'name',

                            'category_id',
                            'cover_image_id',
                            'description',
                            'is_publish',
                            'content',
                        ]
        );

        $input['is_enabled'] = $request->get('is_enabled', 0);
        $this->postRepository->update($post, $input);
        if ($request->hasFile('cover_image_id')) {
            $file = $request->file('cover_image_id');

            $newImage = $this->fileUploadService->upload(
                'post_image',
                $file,
                [
                    'entity_type' => 'post_image',
                    'entity_id'   => $post->id,
                    'title'       => $request->input('name', ''),
                ]
            );

            if (!empty($newImage)) {
                $oldImage = $post->coverImage;
                if (!empty($oldImage)) {
                    $this->fileUploadService->delete($oldImage);
                }

                $this->postRepository->update($post, ['cover_image_id' => $newImage->id]);
            }
        }

        return redirect()->action('Admin\PostController@show', [$id])
                    ->with('message-success', trans('admin.messages.general.update_success'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int $id
     * @return  \Response
     */
    public function destroy($id)
    {
        /** @var  \App\Models\Post $post */
        $post = $this->postRepository->find($id);
        if( empty($post) ) {
            abort(404);
        }
        $this->postRepository->delete($post);

        return redirect()->action('Admin\PostController@index')
                    ->with('message-success', trans('admin.messages.general.delete_success'));
    }

    public function getImages(PaginationRequest $request)
    {
        $entityId = intval($request->input('post_id', 0));
        $type = $request->input('type', 'post_content_image');

        if ($entityId == 0) {
            $imageIds = $this->postService->getImageIdsFromSession();
            $models   = $this->imageRepository->allByIds($imageIds);
        } else {
            /** @var \App\Models\Image[] $models */
            $models = $this->imageRepository->allByEntityTypeAndEntityId($type, $entityId);
        }

        $result = [];
        foreach ($models as $model) {
            $result[] = [
                'id'    => $model->id,
                'url'   => $model->present()->url(),
                'thumb' => '',
                'tag'   => ''
            ];
        }

        return response()->json($result);
    }

    public function postImage(BaseRequest $request)
    {
        if (!$request->hasFile('file')) {
            // [TODO] ERROR JSON
            abort(400, 'No Image File');
        }

        $type     = $request->input('type', 'post_content_image');
        $entityId = $request->input('post_id', 0);

        $conf = config('file.categories.' . $type);
        if (empty($conf)) {
            abort(400, 'Invalid type: ' . $type);
        }

        $file = $request->file('file');

        $image = $this->fileUploadService->upload(
            'post_content_image',
            $file,
            [
                'entity_type' => $type,
                'entity_id'   => $entityId,
                'title'       => $request->input('title', ''),
            ]
        );


        if ($entityId == 0) {
            $this->postService->addImageIdToSession($image->id);
        }

        return response()->json(
            [
                'id'   => $image->id,
                'link' => $image->present()->url(),
            ]
        );
    }

    public function deleteImage(BaseRequest $request)
    {
        $url = $request->input('src');
        if (empty($url)) {
            abort(400, 'No URL Given');
        }
        $url = basename($url);

        /** @var \App\Models\Image|null $image */
        $image = $this->imageRepository->findByUrl($url);
        if (empty($image)) {
            abort(404);
        }

        $entityId = $request->input('post_id', 0);
        if ($entityId != $image->entity_id) {
            abort(400, 'Article ID Mismatch');
        } else {
            if ($entityId == 0 && !$this->postService->hasImageIdInSession($image->id)) {
                abort(400, 'Entity ID Mismatch');
            }
        }

        $this->fileUploadService->delete($image);

        if ($entityId == 0) {
            $this->postService->removeImageIdFromSession($image->id);
        }

        return response()->json(['status' => 'ok'], 204);
    }

}
