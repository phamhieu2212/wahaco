<?php
namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Menu;
use App\Models\Page;
use App\Models\Post;
use App\Repositories\MenuRepositoryInterface;
use App\Http\Requests\Admin\MenuRequest;
use App\Http\Requests\PaginationRequest;
use App\Repositories\PageRepositoryInterface;
use App\Repositories\PostRepositoryInterface;

class MenuController extends Controller
{
    /** @var  \App\Repositories\MenuRepositoryInterface */
    protected $menuRepository;
    protected $pageRepository;
    protected $postRepository;

    public function __construct(
        MenuRepositoryInterface $menuRepository,
        PageRepositoryInterface $pageRepository,
        PostRepositoryInterface $postRepository
    ) {
        $this->menuRepository = $menuRepository;
        $this->pageRepository = $pageRepository;
        $this->postRepository = $postRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param    \App\Http\Requests\PaginationRequest $request
     * @return  \Response
     */
    public function index(PaginationRequest $request)
    {
        $paginate['limit']      = $request->limit();
        $paginate['offset']     = $request->offset();
        $paginate['order']      = $request->order();
        $paginate['direction']  = $request->direction();
        $paginate['baseUrl']    = action('Admin\MenuController@index');

        $filter = [];
        $keyword = $request->get('keyword');
        if (!empty($keyword)) {
            $filter['query'] = $keyword;
        }

        $count = $this->menuRepository->countByFilter($filter);
        $menus = $this->menuRepository->getByFilter($filter, $paginate['order'], $paginate['direction'], $paginate['offset'], $paginate['limit']);

        return view(
            'pages.admin.' . config('view.admin') . '.menus.index',
            [
                'menus'    => $menus,
                'count'         => $count,
                'paginate'      => $paginate,
                'keyword'       => $keyword
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Response
     */
    public function create()
    {
        return view(
            'pages.admin.' . config('view.admin') . '.menus.edit',
            [
                'isNew'     => true,
                'menu' => $this->menuRepository->getBlankModel(),
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    $request
     * @return  \Response
     */
    public function store(MenuRequest $request)
    {
        $input = $request->only(
            [
                            'id',
                            'type',
                        ]
        );
        Menu::truncate();
        $inputMenu['is_publish'] = 1;
        foreach($input['id'] as $key=>$row)
        {
            if($input['type'][$key] == 1)
            {
                $inputMenu['type'] = 1;
                $inputMenu['page_id'] = $row;
            }
            else
            {
                $inputMenu['type'] = 2;
                $inputMenu['category_id'] = $row;

            }
            $menu = $this->menuRepository->create($inputMenu);
        }



        if( empty($menu) ) {
            return redirect()->back()->with('message-error', trans('admin.errors.general.save_failed'));
        }



        return redirect()->action('Admin\MenuController@index')
            ->with('message-success', trans('admin.messages.general.create_success'));
    }

    /**
     * Display the specified resource.
     *
     * @param    int $id
     * @return  \Response
     */
    public function show($id)
    {
        $menu = $this->menuRepository->find($id);
        if( empty($menu) ) {
            abort(404);
        }

        return view(
            'pages.admin.' . config('view.admin') . '.menus.edit',
            [
                'isNew' => false,
                'menu' => $menu,
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param    int $id
     * @return  \Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    int $id
     * @param            $request
     * @return  \Response
     */
    public function update($id, MenuRequest $request)
    {
        /** @var  \App\Models\Menu $menu */
        $menu = $this->menuRepository->find($id);
        if( empty($menu) ) {
            abort(404);
        }

        $input = $request->only(
            [
                            'name',
                        ]
        );


        $this->menuRepository->update($menu, $input);
        if($menu->type == 1)
        {
            $page = Page::where('menu_id',$id)->first();
            $page = $this->pageRepository->update($page,$input);
        }
        elseif($menu->type == 2)
        {
            $post = Post::where('menu_id',$id)->first();
            $post = $this->postRepository->update($post,$input);
        }
        else
        {
            return redirect()->back()->with('message-error', trans('admin.errors.general.save_failed'));

        }

        return redirect()->action('Admin\MenuController@show', [$id])
                    ->with('message-success', trans('admin.messages.general.update_success'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int $id
     * @return  \Response
     */
    public function destroy($id)
    {
        /** @var  \App\Models\Menu $menu */
        $menu = $this->menuRepository->find($id);
        if( empty($menu) ) {
            abort(404);
        }
        $this->menuRepository->delete($menu);

        return redirect()->action('Admin\MenuController@index')
                    ->with('message-success', trans('admin.messages.general.delete_success'));
    }

}
