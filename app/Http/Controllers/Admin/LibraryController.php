<?php
namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Repositories\LibraryRepositoryInterface;
use App\Http\Requests\Admin\LibraryRequest;
use App\Http\Requests\PaginationRequest;
use App\Services\FileUploadServiceInterface;

class LibraryController extends Controller
{
    /** @var  \App\Repositories\LibraryRepositoryInterface */
    protected $libraryRepository;

    protected $fileUploadService;

    public function __construct(
        LibraryRepositoryInterface $libraryRepository,
        FileUploadServiceInterface $fileUploadService
    ) {
        $this->libraryRepository = $libraryRepository;
        $this->fileUploadService = $fileUploadService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param    \App\Http\Requests\PaginationRequest $request
     * @return  \Response
     */
    public function index(PaginationRequest $request)
    {
        $paginate['limit']      = $request->limit();
        $paginate['offset']     = $request->offset();
        $paginate['order']      = $request->order();
        $paginate['direction']  = $request->direction();
        $paginate['baseUrl']    = action('Admin\LibraryController@index');

        $filter = [];
        $keyword = $request->get('keyword');
        if (!empty($keyword)) {
            $filter['query'] = $keyword;
        }

        $count = $this->libraryRepository->countByFilter($filter);
        $libraries = $this->libraryRepository->getByFilter($filter, $paginate['order'], $paginate['direction'], $paginate['offset'], $paginate['limit']);

        return view(
            'pages.admin.' . config('view.admin') . '.libraries.index',
            [
                'libraries'    => $libraries,
                'count'         => $count,
                'paginate'      => $paginate,
                'keyword'       => $keyword
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Response
     */
    public function create()
    {
        return view(
            'pages.admin.' . config('view.admin') . '.libraries.edit',
            [
                'isNew'     => true,
                'library' => $this->libraryRepository->getBlankModel(),
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    $request
     * @return  \Response
     */
    public function store(LibraryRequest $request)
    {
        $input = $request->only(
            [
                            'cover_image_id',
                            'title',
                        ]
        );

        $library = $this->libraryRepository->create($input);

        if( empty($library) ) {
            return redirect()->back()->with('message-error', trans('admin.errors.general.save_failed'));
        }
        if ($request->hasFile('cover_image_id')) {
            $file = $request->file('cover_image_id');

            $image = $this->fileUploadService->upload(
                'library_image',
                $file,
                [
                    'entity_type' => 'library_image',
                    'entity_id' => $library->id,
                    'title' => $request->input('name', ''),
                ]
            );

            if (!empty($image)) {
                $this->libraryRepository->update($library, ['cover_image_id' => $image->id]);

            }
        }

        return redirect()->action('Admin\LibraryController@index')
            ->with('message-success', trans('admin.messages.general.create_success'));
    }

    /**
     * Display the specified resource.
     *
     * @param    int $id
     * @return  \Response
     */
    public function show($id)
    {
        $library = $this->libraryRepository->find($id);
        if( empty($library) ) {
            abort(404);
        }

        return view(
            'pages.admin.' . config('view.admin') . '.libraries.edit',
            [
                'isNew' => false,
                'library' => $library,
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param    int $id
     * @return  \Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    int $id
     * @param            $request
     * @return  \Response
     */
    public function update($id, LibraryRequest $request)
    {
        /** @var  \App\Models\Library $library */
        $library = $this->libraryRepository->find($id);
        if( empty($library) ) {
            abort(404);
        }

        $input = $request->only(
            [
                            'cover_image_id',
                            'title',
                        ]
        );

        $input['is_enabled'] = $request->get('is_enabled', 0);
        $this->libraryRepository->update($library, $input);
        if ($request->hasFile('cover_image_id')) {
            $file = $request->file('cover_image_id');

            $newImage = $this->fileUploadService->upload(
                'library_image',
                $file,
                [
                    'entity_type' => 'library_image',
                    'entity_id'   => $library->id,
                    'title'       => $request->input('name', ''),
                ]
            );

            if (!empty($newImage)) {
                $oldImage = $library->coverImage;
                if (!empty($oldImage)) {
                    $this->fileUploadService->delete($oldImage);
                }

                $this->libraryRepository->update($library, ['cover_image_id' => $newImage->id]);
            }
        }

        return redirect()->action('Admin\LibraryController@show', [$id])
                    ->with('message-success', trans('admin.messages.general.update_success'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int $id
     * @return  \Response
     */
    public function destroy($id)
    {
        /** @var  \App\Models\Library $library */
        $library = $this->libraryRepository->find($id);
        if( empty($library) ) {
            abort(404);
        }
        $this->libraryRepository->delete($library);

        return redirect()->action('Admin\LibraryController@index')
                    ->with('message-success', trans('admin.messages.general.delete_success'));
    }

}
