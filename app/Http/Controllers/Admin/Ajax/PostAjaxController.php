<?php

namespace App\Http\Controllers\Admin\Ajax;

use App\Repositories\PostRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PostAjaxController extends Controller
{
    protected $postRepository;
    public function __construct(PostRepositoryInterface $postRepository)
    {
        $this->postRepository = $postRepository;
    }
    public function loadPublish($id,$status)
    {
        $menu = $this->postRepository->find($id);
        $menu->is_publish = $status;$menu->save();
        if($status == 1)
        {
            return 'Đã hiện bài viết thành công';
        }
        else
        {
            return 'Đã ẩn bài viết thành công';
        }
    }
}
