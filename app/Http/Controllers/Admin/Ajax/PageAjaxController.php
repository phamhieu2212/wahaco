<?php

namespace App\Http\Controllers\Admin\Ajax;

use App\Repositories\PageRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PageAjaxController extends Controller
{
    protected $pageRepository;
    public function __construct(PageRepositoryInterface $pageRepository)
    {
        $this->pageRepository = $pageRepository;
    }
    public function loadPublish($id,$status)
    {
        $menu = $this->pageRepository->find($id);
        $menu->is_publish = $status;$menu->save();
        if($status == 1)
        {
            return 'Đã hiện trang thành công';
        }
        else
        {
            return 'Đã ẩn trang thành công';
        }
    }
}
