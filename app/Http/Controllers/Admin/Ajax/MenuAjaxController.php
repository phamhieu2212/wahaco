<?php

namespace App\Http\Controllers\Admin\Ajax;

use App\Models\Category;
use App\Models\Menu;
use App\Models\Page;
use App\Repositories\MenuRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MenuAjaxController extends Controller
{
    protected $menuRepository;
    public function __construct(MenuRepositoryInterface $menuRepository)
    {
        $this->menuRepository = $menuRepository;
    }
    public function loadPublish($id,$status)
    {
        $menu = $this->menuRepository->find($id);
        $menu->is_publish = $status;$menu->save();
        if($status == 1)
        {
            return 'Đã hiện menu thành công';
        }
        else
        {
            return 'Đã ẩn menu thành công';
        }
    }

    public function getDetail($type)
    {
        if($type == 1)
        {
            $data = Page::where('parent_id',0)->get();
        }
        elseif($type == 2)
        {
            $data = Category::all();
        }
        else{$data = null;}
        return $data;
    }
}
