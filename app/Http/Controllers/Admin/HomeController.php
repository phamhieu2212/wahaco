<?php
namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Home;
use App\Repositories\HomeRepositoryInterface;
use App\Http\Requests\Admin\HomeRequest;
use App\Http\Requests\PaginationRequest;

class HomeController extends Controller
{
    /** @var  \App\Repositories\HomeRepositoryInterface */
    protected $homeRepository;

    public function __construct(
        HomeRepositoryInterface $homeRepository
    ) {
        $this->homeRepository = $homeRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param    \App\Http\Requests\PaginationRequest $request
     * @return  \Response
     */
    public function index(PaginationRequest $request)
    {
        $paginate['limit']      = $request->limit();
        $paginate['offset']     = $request->offset();
        $paginate['order']      = $request->order();
        $paginate['direction']  = $request->direction();
        $paginate['baseUrl']    = action('Admin\HomeController@index');

        $filter = [];
        $keyword = $request->get('keyword');
        if (!empty($keyword)) {
            $filter['query'] = $keyword;
        }

        $count = $this->homeRepository->countByFilter($filter);
        $homes = $this->homeRepository->getByFilter($filter, $paginate['order'], $paginate['direction'], $paginate['offset'], $paginate['limit']);

        return view(
            'pages.admin.' . config('view.admin') . '.homes.index',
            [
                'homes'    => $homes,
                'count'         => $count,
                'paginate'      => $paginate,
                'keyword'       => $keyword
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Response
     */
    public function create()
    {
        return view(
            'pages.admin.' . config('view.admin') . '.homes.edit',
            [
                'isNew'     => true,
                'home' => $this->homeRepository->getBlankModel(),
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    $request
     * @return  \Response
     */
    public function store(HomeRequest $request)
    {
        $countHome = Home::all()->count();
        if($countHome == 3)
        {
            return redirect()->back()->with('message-error', trans('admin.errors.general.save_failed'));
        }
        $input = $request->only(
            [
                            'type',
                            'id',
                            'title',
                        ]
        );
        if($input['type'] == 1 and $input['id'] != 0)
        {
            $input['page_id'] = $input['id'];
        }
        elseif($input['type'] ==2 and $input['id'] != 0)
        {
            $input['category_id'] = $input['id'];
        }
        else
        {
            return redirect()->back()->with('message-error', trans('admin.errors.general.save_failed'));
        }
        $home = $this->homeRepository->create($input);

        if( empty($home) ) {
            return redirect()->back()->with('message-error', trans('admin.errors.general.save_failed'));
        }

        return redirect()->action('Admin\HomeController@index')
            ->with('message-success', trans('admin.messages.general.create_success'));
    }

    /**
     * Display the specified resource.
     *
     * @param    int $id
     * @return  \Response
     */
    public function show($id)
    {
        $home = $this->homeRepository->find($id);
        if( empty($home) ) {
            abort(404);
        }

        return view(
            'pages.admin.' . config('view.admin') . '.homes.edit',
            [
                'isNew' => false,
                'home' => $home,
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param    int $id
     * @return  \Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    int $id
     * @param            $request
     * @return  \Response
     */
    public function update($id, HomeRequest $request)
    {
        /** @var  \App\Models\Home $home */
        $home = $this->homeRepository->find($id);
        if( empty($home) ) {
            abort(404);
        }

        $input = $request->only(
            [
                            'type',
                            'id',
                            'title',
                        ]
        );
        if($input['type'] == 1 and $input['id'] != 0)
        {
            $input['page_id'] = $input['id'];
        }
        elseif($input['type'] ==2 and $input['id'] != 0)
        {
            $input['category_id'] = $input['id'];
        }
        else
        {
            return redirect()->back()->with('message-error', trans('admin.errors.general.save_failed'));
        }

        $this->homeRepository->update($home, $input);

        return redirect()->action('Admin\HomeController@show', [$id])
                    ->with('message-success', trans('admin.messages.general.update_success'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int $id
     * @return  \Response
     */
    public function destroy($id)
    {
        /** @var  \App\Models\Home $home */
        $home = $this->homeRepository->find($id);
        if( empty($home) ) {
            abort(404);
        }
        $this->homeRepository->delete($home);

        return redirect()->action('Admin\HomeController@index')
                    ->with('message-success', trans('admin.messages.general.delete_success'));
    }

}
