<?php
namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Repositories\LogoRepositoryInterface;
use App\Http\Requests\Admin\LogoRequest;
use App\Http\Requests\PaginationRequest;
use App\Services\FileUploadServiceInterface;

class LogoController extends Controller
{
    /** @var  \App\Repositories\LogoRepositoryInterface */
    protected $logoRepository;
    protected $fileUploadService;

    public function __construct(
        LogoRepositoryInterface $logoRepository,
        FileUploadServiceInterface $fileUploadService
    ) {
        $this->logoRepository = $logoRepository;
        $this->fileUploadService = $fileUploadService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param    \App\Http\Requests\PaginationRequest $request
     * @return  \Response
     */
    public function index(PaginationRequest $request)
    {
        $paginate['limit']      = $request->limit();
        $paginate['offset']     = $request->offset();
        $paginate['order']      = $request->order();
        $paginate['direction']  = $request->direction();
        $paginate['baseUrl']    = action('Admin\LogoController@index');

        $filter = [];
        $keyword = $request->get('keyword');
        if (!empty($keyword)) {
            $filter['query'] = $keyword;
        }

        $count = $this->logoRepository->countByFilter($filter);
        $logos = $this->logoRepository->getByFilter($filter, $paginate['order'], $paginate['direction'], $paginate['offset'], $paginate['limit']);

        return view(
            'pages.admin.' . config('view.admin') . '.logos.index',
            [
                'logos'    => $logos,
                'count'         => $count,
                'paginate'      => $paginate,
                'keyword'       => $keyword
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Response
     */
    public function create()
    {
        return view(
            'pages.admin.' . config('view.admin') . '.logos.edit',
            [
                'isNew'     => true,
                'logo' => $this->logoRepository->getBlankModel(),
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    $request
     * @return  \Response
     */
    public function store(LogoRequest $request)
    {
        $input = $request->only(
            [
                            'cover_image_id',
                            'title',
                        ]
        );

        $input['is_enabled'] = $request->get('is_enabled', 0);
        $logo = $this->logoRepository->create($input);

        if( empty($logo) ) {
            return redirect()->back()->with('message-error', trans('admin.errors.general.save_failed'));
        }
        if ($request->hasFile('cover_image_id')) {
            $file = $request->file('cover_image_id');

            $image = $this->fileUploadService->upload(
                'logo_image',
                $file,
                [
                    'entity_type' => 'logo_image',
                    'entity_id' => $logo->id,
                    'title' => $request->input('name', ''),
                ]
            );

            if (!empty($image)) {
                $this->logoRepository->update($logo, ['cover_image_id' => $image->id]);

            }
        }

        return redirect()->action('Admin\LogoController@index')
            ->with('message-success', trans('admin.messages.general.create_success'));
    }

    /**
     * Display the specified resource.
     *
     * @param    int $id
     * @return  \Response
     */
    public function show($id)
    {
        $logo = $this->logoRepository->find($id);
        if( empty($logo) ) {
            abort(404);
        }

        return view(
            'pages.admin.' . config('view.admin') . '.logos.edit',
            [
                'isNew' => false,
                'logo' => $logo,
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param    int $id
     * @return  \Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    int $id
     * @param            $request
     * @return  \Response
     */
    public function update($id, LogoRequest $request)
    {
        /** @var  \App\Models\Logo $logo */
        $logo = $this->logoRepository->find($id);
        if( empty($logo) ) {
            abort(404);
        }

        $input = $request->only(
            [
                            'cover_image_id',
                            'title',
                        ]
        );

        $this->logoRepository->update($logo, $input);

        if ($request->hasFile('cover_image_id')) {
            $file = $request->file('cover_image_id');

            $newImage = $this->fileUploadService->upload(
                'logo_image',
                $file,
                [
                    'entity_type' => 'logo_image',
                    'entity_id'   => $logo->id,
                    'title'       => $request->input('name', ''),
                ]
            );

            if (!empty($newImage)) {
                $oldImage = $logo->coverImage;
                if (!empty($oldImage)) {
                    $this->fileUploadService->delete($oldImage);
                }

                $this->logoRepository->update($logo, ['cover_image_id' => $newImage->id]);
            }
        }

        return redirect()->action('Admin\LogoController@show', [$id])
                    ->with('message-success', trans('admin.messages.general.update_success'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int $id
     * @return  \Response
     */
    public function destroy($id)
    {
        /** @var  \App\Models\Logo $logo */
        $logo = $this->logoRepository->find($id);
        if( empty($logo) ) {
            abort(404);
        }
        $this->logoRepository->delete($logo);

        return redirect()->action('Admin\LogoController@index')
                    ->with('message-success', trans('admin.messages.general.delete_success'));
    }

}
