<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Library;
use App\Models\Post;
use App\Repositories\CategoryRepositoryInterface;
use App\Repositories\HomeRepositoryInterface;
use App\Repositories\LogoRepositoryInterface;
use App\Repositories\MenuRepositoryInterface;
use App\Repositories\PageRepositoryInterface;
use App\Repositories\PostRepositoryInterface;

class IndexController extends Controller
{
    protected $menuRepository;
    protected $pageRepository;
    protected $homeRepository;
    protected $categoryRepository;
    protected $postRepository;
    protected $logoRepository;
    public function __construct(
        MenuRepositoryInterface $menuRepository,
        PageRepositoryInterface $pageRepository,
        CategoryRepositoryInterface $categoryRepository,
        PostRepositoryInterface $postRepository,
        HomeRepositoryInterface $homeRepository,
        LogoRepositoryInterface $logoRepository
    )
    {
        $this->menuRepository = $menuRepository;
        $this->pageRepository = $pageRepository;
        $this->categoryRepository = $categoryRepository;
        $this->postRepository = $postRepository;
        $this->homeRepository = $homeRepository;
        $this->logoRepository = $logoRepository;
    }
    public function index()
    {

        return view('pages.web.home', [
            'menus' => $this->menuRepository->all(),
            'homes' => $this->homeRepository->all(),
            'logos' => $this->logoRepository->all()
        ]);
    }
    public function detail($type,$id)
    {
        if($type == 1)
        {
            return view('pages.web.detail-page', [
                'menus' => $this->menuRepository->all(),
                'page'  => $this->pageRepository->find($id),
                'logos' => $this->logoRepository->all()
            ]);
        }
        if($type == 2)
        {
            return view('pages.web.detail-post', [
                'menus' => $this->menuRepository->all(),
                'post'  => $this->postRepository->find($id),
                'logos' => $this->logoRepository->all()
            ]);
        }


    }
    public function category($id)
    {
        return view('pages.web.category', [
            'menus' => $this->menuRepository->all(),
            'category'  => $this->categoryRepository->find($id),
            'posts'     => Post::where('category_id',$id)->paginate(5),
            'logos' => $this->logoRepository->all()
        ]);


    }

    public function library()
    {
        return view('pages.web.library', [
            'menus' => $this->menuRepository->all(),
            'libraries'     => Library::paginate(5),
            'logos' => $this->logoRepository->all()
        ]);
    }
}
