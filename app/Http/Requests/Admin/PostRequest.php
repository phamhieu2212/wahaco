<?php namespace App\Http\Requests\Admin;

use App\Http\Requests\BaseRequest;
use App\Repositories\PostRepositoryInterface;

class PostRequest extends BaseRequest
{

    /** @var \App\Repositories\PostRepositoryInterface */
    protected $postRepository;

    public function __construct(PostRepositoryInterface $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->postRepository->rules();
    }

    public function messages()
    {
        return $this->postRepository->messages();
    }

}
