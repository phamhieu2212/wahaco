<?php namespace App\Http\Requests\Admin;

use App\Http\Requests\BaseRequest;
use App\Repositories\LibraryRepositoryInterface;

class LibraryRequest extends BaseRequest
{

    /** @var \App\Repositories\LibraryRepositoryInterface */
    protected $libraryRepository;

    public function __construct(LibraryRepositoryInterface $libraryRepository)
    {
        $this->libraryRepository = $libraryRepository;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->libraryRepository->rules();
    }

    public function messages()
    {
        return $this->libraryRepository->messages();
    }

}
