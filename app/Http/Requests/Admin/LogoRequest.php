<?php namespace App\Http\Requests\Admin;

use App\Http\Requests\BaseRequest;
use App\Repositories\LogoRepositoryInterface;

class LogoRequest extends BaseRequest
{

    /** @var \App\Repositories\LogoRepositoryInterface */
    protected $logoRepository;

    public function __construct(LogoRepositoryInterface $logoRepository)
    {
        $this->logoRepository = $logoRepository;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->logoRepository->rules();
    }

    public function messages()
    {
        return $this->logoRepository->messages();
    }

}
