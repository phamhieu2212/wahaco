<?php namespace App\Http\Requests\Admin;

use App\Http\Requests\BaseRequest;
use App\Repositories\PageRepositoryInterface;

class PageRequest extends BaseRequest
{

    /** @var \App\Repositories\PageRepositoryInterface */
    protected $pageRepository;

    public function __construct(PageRepositoryInterface $pageRepository)
    {
        $this->pageRepository = $pageRepository;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->pageRepository->rules();
    }

    public function messages()
    {
        return $this->pageRepository->messages();
    }

}
