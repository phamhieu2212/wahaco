<?php namespace App\Http\Requests\Admin;

use App\Http\Requests\BaseRequest;
use App\Repositories\MenuRepositoryInterface;

class MenuRequest extends BaseRequest
{

    /** @var \App\Repositories\MenuRepositoryInterface */
    protected $menuRepository;

    public function __construct(MenuRepositoryInterface $menuRepository)
    {
        $this->menuRepository = $menuRepository;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->menuRepository->rules();
    }

    public function messages()
    {
        return $this->menuRepository->messages();
    }

}
