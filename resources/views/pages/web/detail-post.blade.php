@extends('pages.web.layout.app',['menu' => 'menus'] )
@section('title')
    Công ty CP đầu tư Công nghệ và Xây dựng WAHACO Việt Nam
@stop
@section('content')
    <div id="breadcrumbs">
        <a class="home" href="{!! action('Web\IndexController@index') !!}">Trang chủ</a> &raquo;<a href="{!! action('Web\IndexController@category',[$post->category->id]) !!}">{{$post->category->name}}</a>
    </div>
    <div id="aside">
        <div class="title-topbar">
            <div class="logo-topbar">
                <img src="{{asset('/static/web/wahaco/images/logo.png')}}">
            </div>
            <div class="text-topbar">
                <div>Menu</div>
            </div>
        </div>
        <div style="clear: both;"></div>
        <div class="widget">
            <ul class="menu-sub-product">
                <li class="menus-2">
                    <a class="title" href="{!! action('Web\IndexController@category',[$post->category->id]) !!}">{{$post->category->name}}</a>
                </li>
            </ul>
        </div>
        <h4 class="su">Menu</h4>
        <div id="support"></div>
        <div class="qc-items" id="qc-items-3">
            <div class="qc-item">
                <a target="_blank" href="{!! action('Web\IndexController@library') !!}">
                    <img width="195" src="{{asset('/static/web/wahaco/images/thau-dien(2).jpg')}}" alt="Banner 1" height="200" title="Banner 1" />
                </a>
            </div>
        </div>
    </div>
    <div id="bodycontent">
        <div class="box-content">
            <div class="title-topbar">
                <div class="logo-topbar">
                    <img src="{{asset('/static/web/wahaco/images/logo.png')}}">
                </div>
                <div class="text-topbar">
                    <div>{{$post->category->name}}</div>
                </div>
            </div>
            <div id="entry">
                <h1 class="entry-title">{{$post->name}}</h1>
                {!!  $post->content !!}
            </div>
            <div class="title-topbar">
                <div class="logo-topbar">
                    <img src="{{asset('/static/web/wahaco/images/logo.png')}}">
                </div>
                <div class="text-topbar">
                    <div>Tin cùng chủ đề</div>
                </div>
            </div>
            <div class="news-others">
                <ul>
                    @foreach($post->category->post as $key=>$row)
                        @if($key <5)
                        <li>
                            <a href="{!! action('Web\IndexController@detail',[2,$row->id]) !!}">{{$row->name}}</a>
                        </li>
                        @endif

                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    <div class="clear"></div>
@stop