<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>@yield('title') </title>
    <link rel="stylesheet" type="text/css" href="{{asset('/static/web/wahaco/css/style.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('/static/web/wahaco/css/colorbox.css')}}" />
    <script type="text/javascript" src="{{asset('/static/web/wahaco/js/jquery.min.js')}}"></script>

    <script type="text/javascript" src="{{asset('/static/web/wahaco/js/jquery.colorbox.js')}}"></script>
</head>
<body>
<div id="box-wrap">
    <div id="box">
        <div id="header">
            <div id="logo">
                <a href="#" title=" "> </a>
                <div id="description"></div>
            </div>
        </div>
        <div id="nav">
            <ul>
                <li class="first"><a class="nav-home" href="{!! action('Web\IndexController@index') !!}">Trang chủ</a></li>
                @foreach($menus as $menu)
                    @if($menu->type == 1)
                        <li>
                            <a href="{!! action('Web\IndexController@detail',[1,$menu->page_id]) !!}">{{$menu->page->name}}</a>
                                <ul>
                                    @foreach($menu->page->children as $row)
                                    <li><a href="{!! action('Web\IndexController@detail',[1,$row->id]) !!}">{{$row->name}}</a></li>
                                    @endforeach
                                </ul>

                        </li>
                    @else
                        <li><a href="{!! action('Web\IndexController@category',[$menu->category_id]) !!}">{{$menu->category->name}}</a></li>
                    @endif

                @endforeach
            </ul>
        </div>
        <div id="sl">
            <img src="{{asset('/static/web/wahaco/images/sl1.png')}}" alt="Slider" title="Slider" />
        </div>
        <div id="main-body">
            @yield('content')
        </div>
        <div style="padding: 0 20px; background: #fff">
            <div id="slider-bottom">
                @foreach($logos as $logo)
                <a target="_blank" href="#">
                    <img width="195" src="{!! $logo->present()->coverImage()->present()->url !!}" alt="{{$logo->title}}" height="80" title="{{$logo->title}}" />
                </a>
                @endforeach
            </div>
        </div>
        <div id="footer-nav">
            <a class="a" href="{!! action('Web\IndexController@index') !!}">Trang chủ</a>
            @foreach($menus as $menu)
                @if($menu->type == 1)
                    <a href="{!! action('Web\IndexController@detail',[1,$menu->page_id]) !!}">{{$menu->page->name}}</a>
                @else
                    <a href="{!! action('Web\IndexController@category',[$menu->category_id]) !!}">{{$menu->category->name}}</a>
                @endif
            @endforeach
        </div>
        <div id="footer">
            <div class="copyright">
                <p><strong>Công ty CP đầu tư Công nghệ và Xây dựng WAHACO Việt Nam</strong></p>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="{{asset('/static/web/wahaco/js/jquery.imageScroller.js')}}"></script>
<script type="text/javascript">
    $(
            function () {
                $( '#slider-bottom' ).imageScroller( {loading:'Wait please...'} );
            }
    );
</script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5bf460f840105007f378de76/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>
<!--End of Tawk.to Script-->
</body>
</html>