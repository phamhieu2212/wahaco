@extends('pages.web.layout.app',['menu' => 'menus'] )
@section('title')
    Công ty CP đầu tư Công nghệ và Xây dựng WAHACO Việt Nam
@stop
@section('content')
    <div id="aside">
        <h4 class="su">Menu</h4>
        <div id="support"></div>
        <div class="qc-items" id="qc-items-3">
            <div class="qc-item"><a target="_blank" href="{!! action('Web\IndexController@library') !!}"><img width="195" src="{{asset('/static/web/wahaco/images/thau-dien(2).jpg')}}" alt="Banner 1" height="200" title="Banner 1" /></a></div>
        </div>
    </div>
    <div id="bodycontent">
        @foreach($homes as $home)
            @if($home->type == 1)
        <div class="box-content">
            <div class="title-topbar">
                <div class="logo-topbar">
                    <img src="{{asset('/static/web/wahaco/images/logo.png')}}">
                </div>
                <div class="text-topbar">
                    <div>{{$home->title}}</div>
                </div>
            </div>
            <div class="page-detail">
                <table style="width: 700px;">
                    <tbody>
                    <tr>
                        <td colspan="2">
                            {{$home->page->description}}
                            <p><strong><a href="{!! action('Web\IndexController@detail',[1,$home->page->id]) !!}">Xem Thêm</a></strong></p>
                        </td>
                        <td width="200">.
                            @if( !empty($home->page->present()->coverImage()) )
                                <img id="cover-image-preview" src="{!! $home->page->present()->coverImage()->present()->url !!}" width="190" height="150" align="right"/>
                            @else
                                <img id="cover-image-preview" src="{!! \URLHelper::asset('img/no_image.jpg', 'common') !!}" width="190" height="150" align="right"/>
                            @endif
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
            @endif
        @endforeach
            @foreach($homes as $home)
                @if($home->type == 2)
        <div id="news-home">
            <div class="cl">
                <div class="title-topbar">
                    <div class="logo-topbar">
                        <img src="{{asset('/static/web/wahaco/images/logo.png')}}">
                    </div>
                    <div class="text-topbar">
                        <div>{{$home->title}}</div>
                    </div>
                </div>
                <div class="clear"></div>
                <h2><a href="{!! action('Web\IndexController@detail',[2,@$home->category->post[0]->id]) !!}" class="title">{{@$home->category->post[0]['name']}}</a></h2>
                <a href="{!! action('Web\IndexController@detail',[2,@$home->category->post[0]->id]) !!}" class="thumb">
                    @if( !empty(@$home->category->post[0]->present()->coverImage()) )
                        <img  src="{!! @$home->category->post[0]->present()->coverImage()->present()->url !!}" />
                    @else
                        <img  src="{!! \URLHelper::asset('img/no_image.jpg', 'common') !!}" />
                    @endif
                </a>
                <div class="extact">{{@$home->category->post[0]['description']}}</div>
                <div class="clear"></div>
                <ul>
                    @foreach($home->category->post as $keyPost=>$post)
                        @if($keyPost != 0 and $keyPost <4)
                    <li>
                        <a href="{!! action('Web\IndexController@detail',[2,$post->id]) !!}">{{$post->name}}</a>
                    </li>@endif

                    @endforeach
                </ul>
            </div>
        </div>
                @endif
                @endforeach
    </div>
    <div class="clear"></div>
@stop
