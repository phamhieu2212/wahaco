@extends('pages.web.layout.app',['menu' => 'menus'] )
@section('title')
    Công ty CP đầu tư Công nghệ và Xây dựng WAHACO Việt Nam
@stop
@section('content')
    <div id="breadcrumbs">
        <a class="home" href="{!! action('Web\IndexController@index') !!}">Trang chủ</a> &raquo;<a href="{!! action('Web\IndexController@detail',[1,$page->id]) !!}">{{$page->name}}</a>
    </div>
    <div id="aside">
        <div class="title-topbar">
            <div class="logo-topbar">
                <img src="{{asset('/static/web/wahaco/images/logo.png')}}">
            </div>
            <div class="text-topbar">
                <div>Menu</div>
            </div>
        </div>
        <div style="clear: both;"></div>
        <div class="widget">
            <ul class="menu-sub-product">
                @foreach($page->children as $row)
                <li class="menus-2">
                    <a class="title" href="{!! action('Web\IndexController@detail',[1,$row->id]) !!}">{{$row->name}}</a>
                </li>
                @endforeach
            </ul>
        </div>
        <h4 class="su">Menu</h4>
        <div id="support"></div>
        <div class="qc-items" id="qc-items-3">
            <div class="qc-item">
                <a target="_blank" href="{!! action('Web\IndexController@library') !!}">
                    <img width="195" src="{{asset('/static/web/wahaco/images/thau-dien(2).jpg')}}" alt="Banner 1" height="200" title="Banner 1" />
                </a>
            </div>
        </div>
    </div>
    <div id="bodycontent">
        <div class="box-content">
            <div class="title-topbar">
                <div class="logo-topbar">
                    <img src="{{asset('/static/web/wahaco/images/logo.png')}}">
                </div>
                <div class="text-topbar">
                    <div>{{$page->name}}</div>
                </div>
            </div>
            <div id="entry">
                {!!  $page->content !!}
            </div>
        </div>
    </div>
    <div class="clear"></div>
@stop