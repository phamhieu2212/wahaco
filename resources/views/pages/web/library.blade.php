@extends('pages.web.layout.app',['menu' => 'menus'] )
@section('title')
    Công ty CP đầu tư Công nghệ và Xây dựng WAHACO Việt Nam
@stop
@section('content')
    <div id="breadcrumbs">
        <a class="home" href="{!! action('Web\IndexController@index') !!}">Trang chủ</a> &raquo;<a href="#">Phòng ảnh</a>
    </div>
    <div id="aside">
        <div class="widget">
            <ul class="menu-news-600 menu-brand" id="menu-news-600">
                <li class="menus-1  first">
                    <span class="folder catename">
                        <a id="menu-52" href="{!! action('Web\IndexController@library') !!}">Phòng ảnh</a>
                    </span>
                </li>
            </ul>
            <div class="clear"></div>
        </div>
        <h4 class="su">Menu</h4>
        <div id="support"></div>
        <div class="qc-items" id="qc-items-3">
            <div class="qc-item">
                <a target="_blank" href="{!! action('Web\IndexController@library') !!}">
                    <img width="195" src="{{asset('/static/web/wahaco/images/thau-dien(2).jpg')}}" alt="Banner 1" height="200" title="Banner 1" />
                </a>
            </div>
        </div>
    </div>
    <div id="bodycontent">
        <div class="box-content">
            <div class="title-topbar">
                <div class="logo-topbar">
                    <img src="{{asset('/static/web/wahaco/images/logo.png')}}">
                </div>
                <div class="text-topbar">
                    <div>Phòng ảnh</div>
                </div>
            </div>
            <div class="cate-items-video">
                @foreach($libraries as $library)
                <div class="cate-item">
                    <a href="{!! $library->present()->coverImage()->present()->url !!}" class="thumb cboxElement">
                        <img src="{!! $library->present()->coverImage()->present()->url !!}" alt="{{$library->title}}" title="{{$library->title}}">
                    </a>
                    <h2>
                        <a class="title">{{$library->title}}</a>
                    </h2>
                </div>
                @endforeach
                <div class="clear"></div>
                {{ $libraries->links() }}
            </div>
        </div>
    </div>
    <div class="clear"></div>
    <script type="text/javascript">
        $(
                function () {
                    $( '#slider-bottom' ).imageScroller( {loading:'Wait please...'} );
                    $( '.cate-item a.thumb' ).colorbox({rel:'thumb'});
                }
        );
    </script>
@stop