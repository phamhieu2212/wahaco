@extends('pages.web.layout.app',['menu' => 'menus'] )
@section('title')
    Công ty CP đầu tư Công nghệ và Xây dựng WAHACO Việt Nam
@stop
@section('content')
    <div id="breadcrumbs">
        <a class="home" href="{!! action('Web\IndexController@index') !!}">Trang chủ</a> &raquo;<a href="{!! action('Web\IndexController@category',[$category->id]) !!}">{{$category->name}}</a>
    </div>
    <div id="aside">
        <div class="title-topbar">
            <div class="logo-topbar">
                <img src="{{asset('/static/web/wahaco/images/logo.png')}}">
            </div>
            <div class="text-topbar">
                <div>Menu</div>
            </div>
        </div>
        <div style="clear: both;"></div>
        <div class="widget">
            <ul class="menu-sub-product">
                <li class="menus-2">
                    <a class="title" href="{!! action('Web\IndexController@category',[$category->id]) !!}">{{$category->name}}</a>
                </li>
            </ul>
        </div>
        <h4 class="su">Menu</h4>
        <div id="support"></div>
        <div class="qc-items" id="qc-items-3">
            <div class="qc-item">
                <a target="_blank" href="{!! action('Web\IndexController@library') !!}">
                    <img width="195" src="{{asset('/static/web/wahaco/images/thau-dien(2).jpg')}}" alt="Banner 1" height="200" title="Banner 1" />
                </a>
            </div>
        </div>
    </div>
    <div id="bodycontent">
        <div class="box-content">
            <div class="title-topbar">
                <div class="logo-topbar">
                    <img src="{{asset('/static/web/wahaco/images/logo.png')}}">
                </div>
                <div class="text-topbar">
                    <div>{{$category->name}}</div>
                </div>
            </div>
            <div class="cate-items">
                @foreach($posts as $row)
                <div class="cate-item">
                    <a href="{!! action('Web\IndexController@detail',[2,$row->id]) !!}" class="thumb">
                        @if( !empty($row->present()->coverImage()) )
                            <img id="cover-image-preview" style="max-width: 100%;" src="{!! $row->present()->coverImage()->present()->url !!}" alt="" class="margin"/>
                        @else
                            <img id="cover-image-preview" style="max-width: 100%;" src="{!! \URLHelper::asset('img/no_image.jpg', 'common') !!}" alt="" class="margin"/>
                        @endif
                    </a>
                    <h2><a href="{!! action('Web\IndexController@detail',[2,$row->id]) !!}" class="title">{{$row->name}}</a></h2>
                    <div class="extact">
                        {{$row->description}}
                    </div>
                </div>
                @endforeach


                <div class="clear"></div>

            </div>
            {{ $posts->links() }}
        </div>
    </div>
    <div class="clear"></div>
@stop