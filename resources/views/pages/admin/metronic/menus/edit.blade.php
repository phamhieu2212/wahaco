@extends('pages.admin.metronic.layout.application',['menu' => 'menus'] )

@section('metadata')
@stop

@section('styles')
    <style>
        .row {
            margin-bottom: 15px;
        }
    </style>
@stop

@section('scripts')
    <script src="{!! \URLHelper::asset('libs/metronic/demo/default/custom/crud/forms/validation/form-controls.js', 'admin') !!}"></script>
    <script>
        $(document).ready(function () {
            $("#typeForm").change(function () {
                $('#nameForm').find('option').remove();
                if($("#typeForm").val() == 0)
                {
                    alert('Vui long chon loai')
                }
                else
                {
                    var type = $("#typeForm").val()
                    $("#nameForm").append('<option value="0">Vui lòng chọn</option>');
                    $.ajax({

                        type: "GET",
                        url: window.location.origin + '/admin/ajax/menus/getDetail/'+type,
                        success: function (data) {
                            for (var i = 0; i <= data.length - 1; i++) {
                                $("#nameForm").append('<option value="' + data[i].id + '">' + data[i].name + '</option>');
                            }



                        }

                    });
                }

            });
            $("#addButton").click(function() {
                if($("#typeForm").val() == 1)
                {
                    var type  = 1 ;
                    var typeName  = 'Trang' ;
                }
                if($("#typeForm").val() == 2)
                {
                    var type  = 2;
                    var typeName  = 'Danh mục' ;
                }

                var id       = $('#nameForm option:selected').val();
                var name      = $('#nameForm option:selected').text();
                if(id != 0)
                {
                    $("#bodyTable").append('' +
                            '<tr>' +
                            '<td>' +
                            '<input class="type" name="type[]" type="hidden" value="'+type+'" size="30" />' +
                            typeName+
                            '</td>'+

                            '<td>'+
                            '<input type="hidden" name="id[]" value="'+id+'" size="30" />' +
                            name+
                            '</td>'+
                            '<td>' +
                            '<button type="button" class="btn btn-danger m-btn btn-xs m-btn m-btn--icon delBut">Xoá</button>'+
                            '</td>'+
                            '</tr>' +
                            '');
                }

            });
            $('#index-table').on('click', '.delBut', function(event) {

                $(this).closest("tr").remove();

            });
            $('#cover-image').change(function (event) {
                $('#cover-image-preview').attr('src', URL.createObjectURL(event.target.files[0]));
            });

            $('.datetime-picker').datetimepicker({
                todayHighlight: true,
                autoclose: true,
                pickerPosition: 'bottom-left',
                format: 'yyyy/mm/dd hh:ii'
            });
        });
    </script>
@stop

@section('title')
    @if($isNew)
        {{'Thêm mới Menu'}}
    @else
        {{'Sửa Menu'}}
    @endif
@stop

@section('header')
    Quản lý Menu
@stop

@section('breadcrumb')
    <li class="m-nav__separator"> / </li>
    <li class="m-nav__item">
        <a href="{!! action('Admin\MenuController@index') !!}" class="m-nav__link">
            Danh sách Menu
        </a>
    </li>

    @if( $isNew )
        <li class="m-nav__separator"> / </li>
        <li class="m-nav__item">
            Thêm mới
        </li>
    @else
        <li class="m-nav__separator"> / </li>
        <li class="m-nav__item">
            {{ $menu->id }}
        </li>
    @endif
@stop

@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Thêm mới
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <a href="{!! action('Admin\MenuController@index') !!}" class="btn m-btn--pill m-btn--air btn-secondary btn-sm" style="width: 120px;">
                            Quay lại
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="m-portlet__body">
            @if(isset($errors) && count($errors))
                {{ $errs = $errors->all() }}
                <div class="m-alert m-alert--outline alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                    <strong>
                        Error !!!
                    </strong>
                    <ul>
                        @foreach($errs as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form class="m-form m-form--fit" action="@if($isNew){!! action('Admin\MenuController@store') !!}@else{!! action('Admin\MenuController@update', [$menu->id]) !!}@endif" method="POST">
                @if( !$isNew ) <input type="hidden" name="_method" value="PUT"> @endif
                {!! csrf_field() !!}

                <div class="m-portlet__body" style="padding-top: 0;">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group m-form__group row">
                                <label for="type">Loại</label>
                                <select class="form-control js-example-basic-multiple"  id="typeForm" name="">
                                    <option value="0">Vui lòng chọn loại</option>
                                    <option value="1">Trang</option>
                                    <option value="2">Danh mục bài viết</option>

                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group m-form__group row @if ($errors->has('name')) has-danger @endif">
                                <label for="name">Tên</label>
                                <select class="form-control js-example-basic-multiple"  id="nameForm" name="">

                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <button id="addButton" type="button" class="btn m-btn--pill m-btn--air btn-primary m-btn m-btn--custom">
                                Thêm menu
                            </button>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body wrap-index">
                    <div class="dataTables_wrapper">

                        <div class="row">
                            <div class="col-sm-12 wrap-index-table">
                                <table class="table table-striped- table-bordered table-hover table-checkable" id="index-table">
                                    <thead>
                                    <tr>
                                        <th>Loại</th>
                                        <th>Tên</th>
                                        <th>Hành động</th>
                                    </tr>
                                    </thead>

                                    <tbody id="bodyTable">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions m-form__actions">
                        <div class="row">
                            <div class="col-lg-9 ml-lg-auto">
                                <a href="{!! action('Admin\MenuController@index') !!}" class="btn m-btn--pill btn-secondary m-btn m-btn--custom m-btn--label-accent" style="width: 120px;">
                                    Huỷ
                                </a>
                                <button type="submit" class="btn m-btn--pill m-btn--air btn-primary m-btn m-btn--custom" style="width: 120px;">
                                    Lưu
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop
