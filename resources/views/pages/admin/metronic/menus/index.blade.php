@extends('pages.admin.metronic.layout.application',['menu' => 'menus'] )

@section('metadata')
@stop

@section('styles')
@stop

@section('scripts')
    <script src="{!! \URLHelper::asset('libs/metronic/demo/default/custom/components/base/sweetalert2.js', 'admin') !!}"></script>
    <script src="{!! \URLHelper::asset('metronic/js/delete_item.js', 'admin') !!}"></script>
    <script>
        function loadPublishMenu(obj){
            var id = obj.id.slice(4);
            if ($('#'+obj.id).is(":checked"))

            {
                status = 1;
                $.ajax({

                    type: "GET",
                    url: window.location.origin + '/admin/ajax/menus/publish/' + id+'/'+status,
                    success: function (data) {
                        alert(data)

                    }

                })
            }
            else
            {
                status = 0;
                $.ajax({

                    type: "GET",
                    url: window.location.origin + '/admin/ajax/menus/publish/' +id+'/'+status,
                    success: function (data) {
                        alert(data)

                    }

                })
            }
        }
    </script>
@stop

@section('title')
     Quản lý Menu
@stop

@section('header')
    Danh sách Menu
@stop

@section('breadcrumb')
    <li class="m-nav__separator"> - </li>
    <li class="m-nav__item">
        <a href="" class="m-nav__link">
            <span class="m-nav__link-text">
                Danh sách Menu
            </span>
        </a>
    </li>
@stop

@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Danh sách Menu
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <a href="{!! action('Admin\MenuController@create') !!}" class="btn m-btn--pill m-btn--air btn-outline-success btn-sm">
                            <span>
                                <i class="la la-plus"></i>
                                <span>Sắp xếp Menu</span>
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="m-portlet__body wrap-index">
            <div class="dataTables_wrapper">
                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        Tổng số: {{$count}} kết quả
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <div id="m_table_1_filter" class="dataTables_filter">
                            <form method="get" accept-charset="utf-8" action="{!! action('Admin\MenuController@index') !!}">
                                {!! csrf_field() !!}
                                <div class="m-input-icon m-input-icon--left m-input-icon--right">
                                    <input type="text" name="keyword" id="keyword" value="{{ $keyword }}" class="form-control m-input m-input--pill" placeholder="Tìm kiếm ...">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span>
                                            <i class="la la-search"></i>
                                        </span>
                                    </span>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12 wrap-index-table">
                        <table class="table table-striped- table-bordered table-hover table-checkable" id="index-table">
                            <thead>
                                <tr>
                                    <th style="width: 10px">{!! \PaginationHelper::sort('id', 'STT') !!}</th>
                                    <th>{!! \PaginationHelper::sort('name', 'Tên') !!}</th>
                                    <th>{!! \PaginationHelper::sort('type', 'Loại') !!}</th>
                                    {{--<th>{!! \PaginationHelper::sort('is_publish', 'Trạng thái') !!}</th>--}}
                                    
                                    <th style="width: 40px">Hành động</th>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach( $menus as $menu )
                                    <tr>
                                        <td>{{ $menu->id }}</td>
                                        <td>{{ ($menu->type == 1)?$menu->page->name:$menu->category->name}}</td>
                                        <td>{{ ($menu->type == 1)?"Trang":"Bài viết" }}</td>
                                        {{--<td>--}}
                                            {{--<span class="m-switch m-switch--outline m-switch--success">--}}
                                                {{--<label>--}}
                                                    {{--<input {{($menu->is_publish !=0)?'checked':''}} onclick="loadPublishMenu(this);" id="{{'top_'.$menu->id}}"  type="checkbox">--}}
                                                    {{--<span></span>--}}
                                                {{--</label>--}}
                                            {{--</span>--}}
                                        {{--</td>--}}
                                        <td>
                                            <a href="javascript:;" data-delete-url="{!! action('Admin\MenuController@destroy', $menu->id) !!}" class="btn m--font-danger m-btn--pill m-btn--air no-padding delete-button">
                                                <i class="la la-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row wrap-bottom-pagination">
                    <div class="col-sm-12">
                        {!! \PaginationHelper::render($paginate['order'], $paginate['direction'], $paginate['offset'], $paginate['limit'], $count, $paginate['baseUrl'], ['keyword' => $keyword], 5, 'pages.admin.metronic.shared.bottom-pagination') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
