<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">
    <i class="la la-close"></i>
</button>
<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
    <!-- BEGIN: Aside Menu -->
    <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " m-menu-vertical="1" m-menu-scrollable="0" m-menu-dropdown-timeout="500">
        <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow " style="padding-top: 0;">
            <li class="m-menu__section ">
                <h4 class="m-menu__section-text">
                    MAIN NAVIGATION
                </h4>
                <i class="m-menu__section-icon flaticon-more-v3"></i>
            </li>

            <li class="m-menu__item @if( $menu=='dashboard') m-menu__item--active @endif" aria-haspopup="true">
                <a href="{!! \URL::action('Admin\IndexController@index') !!}" class="m-menu__link ">
                    <i class="m-menu__link-icon flaticon-line-graph"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                @lang('admin.menu.dashboard')
                            </span>
                        </span>
                    </span>
                </a>
            </li>


            @if( $authUser->hasRole(\App\Models\AdminUserRole::ROLE_SUPER_USER) )
                <li class="m-menu__section ">
                    <h4 class="m-menu__section-text">
                        Quản lý người dùng
                    </h4>
                    <i class="m-menu__section-icon flaticon-more-v3"></i>
                </li>

                <li class="m-menu__item @if( $menu=='admin_users') m-menu__item--active @endif" aria-haspopup="true">
                    <a href="{!! \URL::action('Admin\AdminUserController@index') !!}" class="m-menu__link ">
                        <i class="m-menu__link-icon la la-user-secret"></i>
                        <span class="m-menu__link-title">
                            <span class="m-menu__link-wrap">
                                <span class="m-menu__link-text">
                                    Danh sách người dùng
                                </span>
                            </span>
                        </span>
                    </a>
                </li>

            @endif
            @if( $authUser->hasRole(\App\Models\AdminUserRole::ROLE_ADMIN) )
                <li class="m-menu__section ">
                    <h4 class="m-menu__section-text">
                        Quản lý Web
                    </h4>
                    <i class="m-menu__section-icon flaticon-more-v3"></i>
                </li>
                <li class="m-menu__item @if( $menu=='homes') m-menu__item--active @endif" aria-haspopup="true">
                    <a href="{!! \URL::action('Admin\HomeController@index') !!}" class="m-menu__link">
                        <i class="m-menu__link-icon la la-home"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                Quản lý Trang chủ
                            </span>
                        </span>
                    </span>
                    </a>
                </li>
                <li class="m-menu__item @if( $menu=='menus') m-menu__item--active @endif" aria-haspopup="true">
                    <a href="{!! \URL::action('Admin\MenuController@index') !!}" class="m-menu__link">
                        <i class="m-menu__link-icon la la-navicon"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                Quản lý Menu
                            </span>
                        </span>
                    </span>
                    </a>
                </li>

                <li class="m-menu__item @if( $menu=='pages') m-menu__item--active @endif" aria-haspopup="true">
                    <a href="{!! \URL::action('Admin\PageController@index') !!}" class="m-menu__link">
                        <i class="m-menu__link-icon la la-pagelines"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                Quản lý Trang
                            </span>
                        </span>
                    </span>
                    </a>
                </li>
                <li class="m-menu__item @if( $menu=='categories') m-menu__item--active @endif" aria-haspopup="true">
                    <a href="{!! \URL::action('Admin\CategoryController@index') !!}" class="m-menu__link">
                        <i class="m-menu__link-icon la la-archive"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                Quản lý danh mục bài viết
                            </span>
                        </span>
                    </span>
                    </a>
                </li>

                <li class="m-menu__item @if( $menu=='posts') m-menu__item--active @endif" aria-haspopup="true">
                    <a href="{!! \URL::action('Admin\PostController@index') !!}" class="m-menu__link">
                        <i class="m-menu__link-icon la la-sticky-note"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                Quản lý bài viết
                            </span>
                        </span>
                    </span>
                    </a>
                </li>

            @endif

            @if( $authUser->hasRole(\App\Models\AdminUserRole::ROLE_ADMIN) )
                <li class="m-menu__section ">
                    <h4 class="m-menu__section-text">
                        Quản lý thư viện
                    </h4>
                    <i class="m-menu__section-icon flaticon-more-v3"></i>
                </li>
                <li class="m-menu__item @if( $menu=='libraries') m-menu__item--active @endif" aria-haspopup="true">
                    <a href="{!! \URL::action('Admin\LibraryController@index') !!}" class="m-menu__link">
                        <i class="m-menu__link-icon la la-image"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                Quản lý phòng ảnh
                            </span>
                        </span>
                    </span>
                    </a>
                </li>

                <li class="m-menu__item @if( $menu=='logos') m-menu__item--active @endif" aria-haspopup="true">
                    <a href="{!! \URL::action('Admin\LogoController@index') !!}" class="m-menu__link">
                        <i class="m-menu__link-icon la la-bell"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                Quản lý logo
                            </span>
                        </span>
                    </span>
                    </a>
                </li>

            @endif




            <!-- %%SIDEMENU%% -->
        </ul>
    </div>
    <!-- END: Aside Menu -->
</div>