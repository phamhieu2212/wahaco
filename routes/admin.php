<?php

\Route::group(['middleware' => ['admin.values']], function () {

    \Route::group(['middleware' => ['admin.guest']], function () {
        \Route::get('signin', 'Admin\AuthController@getSignIn');
        \Route::post('signin', 'Admin\AuthController@postSignIn');
        \Route::get('forgot-password', 'Admin\PasswordController@getForgotPassword');
        \Route::post('forgot-password', 'Admin\PasswordController@postForgotPassword');
        \Route::get('reset-password/{token}', 'Admin\PasswordController@getResetPassword');
        \Route::post('reset-password', 'Admin\PasswordController@postResetPassword');
    });

    \Route::group(['middleware' => ['admin.auth']], function () {

        \Route::group(['middleware' => ['admin.has_role.super_user']], function () {
            \Route::resource('users', 'Admin\UserController');
            \Route::resource('user-notifications', 'Admin\UserNotificationController');
            \Route::resource('admin-users', 'Admin\AdminUserController');

            \Route::post('articles/preview', 'Admin\ArticleController@preview');
            \Route::get('articles/images', 'Admin\ArticleController@getImages');
            \Route::post('articles/images', 'Admin\ArticleController@postImage');
            \Route::delete('articles/images', 'Admin\ArticleController@deleteImage');
            \Route::resource('articles', 'Admin\ArticleController');

            \Route::delete('images/delete', 'Admin\ImageController@deleteByUrl');
            \Route::resource('images', 'Admin\ImageController');

            \Route::resource('oauth-clients', 'Admin\OauthClientController');
            \Route::resource('logs', 'Admin\LogController');

        });

        \Route::group(['middleware' => ['admin.has_role.admin']], function () {

            \Route::resource('menus', 'Admin\MenuController');
            \Route::resource('admin-user-notifications', 'Admin\AdminUserNotificationController');
            
            \Route::get('load-notification/{offset}', 'Admin\AdminUserNotificationController@loadNotification');
            Route::group([
                'prefix' => 'ajax',
            ], function (){

                Route::get('menus/publish/{id}/{status}', 'Admin\Ajax\MenuAjaxController@loadPublish');
                Route::get('menus/getDetail/{type}', 'Admin\Ajax\MenuAjaxController@getDetail');
                Route::get('pages/publish/{id}/{status}', 'Admin\Ajax\PageAjaxController@loadPublish');
                Route::get('posts/publish/{id}/{status}', 'Admin\Ajax\PostAjaxController@loadPublish');

            });
        });

        \Route::get('/', 'Admin\IndexController@index');

        \Route::get('/me', 'Admin\MeController@index');
        \Route::put('/me', 'Admin\MeController@update');
        \Route::get('/me/notifications', 'Admin\MeController@notifications');

        \Route::post('signout', 'Admin\AuthController@postSignOut');

        \Route::post('pages/preview', 'Admin\PageController@preview');
        \Route::get('pages/images', 'Admin\PageController@getImages');
        \Route::post('pages/images', 'Admin\PageController@postImage');
        \Route::delete('pages/images', 'Admin\PageController@deleteImage');
        \Route::resource('pages', 'Admin\PageController');


        \Route::post('posts/preview', 'Admin\PostController@preview');
        \Route::get('posts/images', 'Admin\PostController@getImages');
        \Route::post('posts/images', 'Admin\PostController@postImage');
        \Route::delete('posts/images', 'Admin\PostController@deleteImage');
        \Route::resource('posts', 'Admin\PostController');
        \Route::resource('categories', 'Admin\CategoryController');
        \Route::resource('homes', 'Admin\HomeController');
        \Route::resource('libraries', 'Admin\LibraryController');
        \Route::resource('logos', 'Admin\LogoController');
        /* NEW ADMIN RESOURCE ROUTE */

    });
});
