<?php

use Illuminate\Database\Seeder;
use App\Models\AdminUser;
use App\Models\AdminUserRole;

class AdminUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $adminUser = factory( AdminUser::class )->create(
            [
                'name'     => 'TestUser',
                'email'    => 'admin@gmail.com',
                'password' => '123456',
            ]
        );

        factory( AdminUserRole::class )->create(
            [
                'admin_user_id' => $adminUser->id,
                'role'          => AdminUserRole::ROLE_SUPER_USER,
            ]
        );
    }
}
