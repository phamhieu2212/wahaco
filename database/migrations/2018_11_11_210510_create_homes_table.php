<?php

use Illuminate\Database\Schema\Blueprint;
use \App\Database\Migration;

class CreatehomesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('homes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->unsignedTinyInteger('type')->default(0);
            $table->unsignedInteger('page_id')->nullable()->default(0);
            $table->unsignedInteger('category_id')->nullable()->default(0);

            // Add some more columns

            $table->timestamps();
        });

        $this->updateTimestampDefaultValue('homes', ['updated_at'], ['created_at']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('homes');
    }
}
