<?php

use Illuminate\Database\Schema\Blueprint;
use \App\Database\Migration;

class CreatepagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->bigIncrements('id');

            // Add some more columns
            $table->string('name');
            $table->unsignedInteger('parent_id')->nullable()->default(0);
            $table->unsignedBigInteger('cover_image_id')->default(0);
            $table->text('description')->nullable();
            $table->boolean('is_publish')->default(true);
            $table->text('content')->nullable();

            $table->timestamps();
        });

        $this->updateTimestampDefaultValue('pages', ['updated_at'], ['created_at']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
