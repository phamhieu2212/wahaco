<?php

use Illuminate\Database\Schema\Blueprint;
use \App\Database\Migration;

class CreatepostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->unsignedInteger('category_id')->nullable()->default(0);
            $table->unsignedBigInteger('cover_image_id')->default(0);
            $table->text('description')->nullable();
            $table->boolean('is_publish')->default(true);
            $table->text('content')->nullable();

            // Add some more columns

            $table->timestamps();
        });

        $this->updateTimestampDefaultValue('posts', ['updated_at'], ['created_at']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
