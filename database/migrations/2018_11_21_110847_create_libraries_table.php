<?php

use Illuminate\Database\Schema\Blueprint;
use \App\Database\Migration;

class CreatelibrariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('libraries', function (Blueprint $table) {
            $table->bigIncrements('id');

            // Add some more columns
            $table->unsignedBigInteger('cover_image_id')->default(0);
            $table->string('title');

            $table->timestamps();
        });

        $this->updateTimestampDefaultValue('libraries', ['updated_at'], ['created_at']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('libraries');
    }
}
