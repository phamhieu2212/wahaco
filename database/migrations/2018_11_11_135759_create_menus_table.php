<?php

use Illuminate\Database\Schema\Blueprint;
use \App\Database\Migration;

class CreatemenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->bigIncrements('id');

            // Add some more columns
            $table->unsignedTinyInteger('type')->default(0);
            $table->unsignedInteger('page_id')->nullable()->default(0);
            $table->unsignedInteger('category_id')->nullable()->default(0);
            $table->boolean('is_publish')->default(true);

            $table->timestamps();
        });

        $this->updateTimestampDefaultValue('menus', ['updated_at'], ['created_at']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
