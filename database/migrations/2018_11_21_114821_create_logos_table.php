<?php

use Illuminate\Database\Schema\Blueprint;
use \App\Database\Migration;

class CreatelogosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logos', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('cover_image_id')->default(0);
            $table->string('title');

            // Add some more columns

            $table->timestamps();
        });

        $this->updateTimestampDefaultValue('logos', ['updated_at'], ['created_at']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logos');
    }
}
