<?php namespace Tests\Services;

use Tests\TestCase;

class PageServiceTest extends TestCase
{

    public function testGetInstance()
    {
        /** @var  \App\Services\PageServiceInterface $service */
        $service = \App::make(\App\Services\PageServiceInterface::class);
        $this->assertNotNull($service);
    }

}
