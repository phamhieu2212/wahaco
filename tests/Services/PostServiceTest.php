<?php namespace Tests\Services;

use Tests\TestCase;

class PostServiceTest extends TestCase
{

    public function testGetInstance()
    {
        /** @var  \App\Services\PostServiceInterface $service */
        $service = \App::make(\App\Services\PostServiceInterface::class);
        $this->assertNotNull($service);
    }

}
