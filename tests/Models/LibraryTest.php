<?php namespace Tests\Models;

use App\Models\Library;
use Tests\TestCase;

class LibraryTest extends TestCase
{

    protected $useDatabase = true;

    public function testGetInstance()
    {
        /** @var  \App\Models\Library $library */
        $library = new Library();
        $this->assertNotNull($library);
    }

    public function testStoreNew()
    {
        /** @var  \App\Models\Library $library */
        $libraryModel = new Library();

        $libraryData = factory(Library::class)->make();
        foreach( $libraryData->toFillableArray() as $key => $value ) {
            $libraryModel->$key = $value;
        }
        $libraryModel->save();

        $this->assertNotNull(Library::find($libraryModel->id));
    }

}
