<?php namespace Tests\Models;

use App\Models\Post;
use Tests\TestCase;

class PostTest extends TestCase
{

    protected $useDatabase = true;

    public function testGetInstance()
    {
        /** @var  \App\Models\Post $post */
        $post = new Post();
        $this->assertNotNull($post);
    }

    public function testStoreNew()
    {
        /** @var  \App\Models\Post $post */
        $postModel = new Post();

        $postData = factory(Post::class)->make();
        foreach( $postData->toFillableArray() as $key => $value ) {
            $postModel->$key = $value;
        }
        $postModel->save();

        $this->assertNotNull(Post::find($postModel->id));
    }

}
