<?php namespace Tests\Models;

use App\Models\Menu;
use Tests\TestCase;

class MenuTest extends TestCase
{

    protected $useDatabase = true;

    public function testGetInstance()
    {
        /** @var  \App\Models\Menu $menu */
        $menu = new Menu();
        $this->assertNotNull($menu);
    }

    public function testStoreNew()
    {
        /** @var  \App\Models\Menu $menu */
        $menuModel = new Menu();

        $menuData = factory(Menu::class)->make();
        foreach( $menuData->toFillableArray() as $key => $value ) {
            $menuModel->$key = $value;
        }
        $menuModel->save();

        $this->assertNotNull(Menu::find($menuModel->id));
    }

}
