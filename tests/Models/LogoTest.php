<?php namespace Tests\Models;

use App\Models\Logo;
use Tests\TestCase;

class LogoTest extends TestCase
{

    protected $useDatabase = true;

    public function testGetInstance()
    {
        /** @var  \App\Models\Logo $logo */
        $logo = new Logo();
        $this->assertNotNull($logo);
    }

    public function testStoreNew()
    {
        /** @var  \App\Models\Logo $logo */
        $logoModel = new Logo();

        $logoData = factory(Logo::class)->make();
        foreach( $logoData->toFillableArray() as $key => $value ) {
            $logoModel->$key = $value;
        }
        $logoModel->save();

        $this->assertNotNull(Logo::find($logoModel->id));
    }

}
