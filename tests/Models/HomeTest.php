<?php namespace Tests\Models;

use App\Models\Home;
use Tests\TestCase;

class HomeTest extends TestCase
{

    protected $useDatabase = true;

    public function testGetInstance()
    {
        /** @var  \App\Models\Home $home */
        $home = new Home();
        $this->assertNotNull($home);
    }

    public function testStoreNew()
    {
        /** @var  \App\Models\Home $home */
        $homeModel = new Home();

        $homeData = factory(Home::class)->make();
        foreach( $homeData->toFillableArray() as $key => $value ) {
            $homeModel->$key = $value;
        }
        $homeModel->save();

        $this->assertNotNull(Home::find($homeModel->id));
    }

}
