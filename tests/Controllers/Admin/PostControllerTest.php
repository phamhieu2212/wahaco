<?php  namespace Tests\Controllers\Admin;

use Tests\TestCase;

class PostControllerTest extends TestCase
{

    protected $useDatabase = true;

    public function testGetInstance()
    {
        /** @var  \App\Http\Controllers\Admin\PostController $controller */
        $controller = \App::make(\App\Http\Controllers\Admin\PostController::class);
        $this->assertNotNull($controller);
    }

    public function setUp()
    {
        parent::setUp();
        $authUser = \App\Models\AdminUser::first();
        $this->be($authUser, 'admins');
    }

    public function testGetList()
    {
        $response = $this->action('GET', 'Admin\PostController@index');
        $this->assertResponseOk();
    }

    public function testCreateModel()
    {
        $this->action('GET', 'Admin\PostController@create');
        $this->assertResponseOk();
    }

    public function testStoreModel()
    {
        $post = factory(\App\Models\Post::class)->make();
        $this->action('POST', 'Admin\PostController@store', [
                '_token' => csrf_token(),
            ] + $post->toArray());
        $this->assertResponseStatus(302);
    }

    public function testEditModel()
    {
        $post = factory(\App\Models\Post::class)->create();
        $this->action('GET', 'Admin\PostController@show', [$post->id]);
        $this->assertResponseOk();
    }

    public function testUpdateModel()
    {
        $faker = \Faker\Factory::create();

        $post = factory(\App\Models\Post::class)->create();

        $name = $faker->name;
        $id = $post->id;

        $post->name = $name;

        $this->action('PUT', 'Admin\PostController@update', [$id], [
                '_token' => csrf_token(),
            ] + $post->toArray());
        $this->assertResponseStatus(302);

        $newPost = \App\Models\Post::find($id);
        $this->assertEquals($name, $newPost->name);
    }

    public function testDeleteModel()
    {
        $post = factory(\App\Models\Post::class)->create();

        $id = $post->id;

        $this->action('DELETE', 'Admin\PostController@destroy', [$id], [
                '_token' => csrf_token(),
            ]);
        $this->assertResponseStatus(302);

        $checkPost = \App\Models\Post::find($id);
        $this->assertNull($checkPost);
    }

}
