<?php  namespace Tests\Controllers\Admin;

use Tests\TestCase;

class HomeControllerTest extends TestCase
{

    protected $useDatabase = true;

    public function testGetInstance()
    {
        /** @var  \App\Http\Controllers\Admin\HomeController $controller */
        $controller = \App::make(\App\Http\Controllers\Admin\HomeController::class);
        $this->assertNotNull($controller);
    }

    public function setUp()
    {
        parent::setUp();
        $authUser = \App\Models\AdminUser::first();
        $this->be($authUser, 'admins');
    }

    public function testGetList()
    {
        $response = $this->action('GET', 'Admin\HomeController@index');
        $this->assertResponseOk();
    }

    public function testCreateModel()
    {
        $this->action('GET', 'Admin\HomeController@create');
        $this->assertResponseOk();
    }

    public function testStoreModel()
    {
        $home = factory(\App\Models\Home::class)->make();
        $this->action('POST', 'Admin\HomeController@store', [
                '_token' => csrf_token(),
            ] + $home->toArray());
        $this->assertResponseStatus(302);
    }

    public function testEditModel()
    {
        $home = factory(\App\Models\Home::class)->create();
        $this->action('GET', 'Admin\HomeController@show', [$home->id]);
        $this->assertResponseOk();
    }

    public function testUpdateModel()
    {
        $faker = \Faker\Factory::create();

        $home = factory(\App\Models\Home::class)->create();

        $name = $faker->name;
        $id = $home->id;

        $home->name = $name;

        $this->action('PUT', 'Admin\HomeController@update', [$id], [
                '_token' => csrf_token(),
            ] + $home->toArray());
        $this->assertResponseStatus(302);

        $newHome = \App\Models\Home::find($id);
        $this->assertEquals($name, $newHome->name);
    }

    public function testDeleteModel()
    {
        $home = factory(\App\Models\Home::class)->create();

        $id = $home->id;

        $this->action('DELETE', 'Admin\HomeController@destroy', [$id], [
                '_token' => csrf_token(),
            ]);
        $this->assertResponseStatus(302);

        $checkHome = \App\Models\Home::find($id);
        $this->assertNull($checkHome);
    }

}
