<?php  namespace Tests\Controllers\Admin;

use Tests\TestCase;

class LibraryControllerTest extends TestCase
{

    protected $useDatabase = true;

    public function testGetInstance()
    {
        /** @var  \App\Http\Controllers\Admin\LibraryController $controller */
        $controller = \App::make(\App\Http\Controllers\Admin\LibraryController::class);
        $this->assertNotNull($controller);
    }

    public function setUp()
    {
        parent::setUp();
        $authUser = \App\Models\AdminUser::first();
        $this->be($authUser, 'admins');
    }

    public function testGetList()
    {
        $response = $this->action('GET', 'Admin\LibraryController@index');
        $this->assertResponseOk();
    }

    public function testCreateModel()
    {
        $this->action('GET', 'Admin\LibraryController@create');
        $this->assertResponseOk();
    }

    public function testStoreModel()
    {
        $library = factory(\App\Models\Library::class)->make();
        $this->action('POST', 'Admin\LibraryController@store', [
                '_token' => csrf_token(),
            ] + $library->toArray());
        $this->assertResponseStatus(302);
    }

    public function testEditModel()
    {
        $library = factory(\App\Models\Library::class)->create();
        $this->action('GET', 'Admin\LibraryController@show', [$library->id]);
        $this->assertResponseOk();
    }

    public function testUpdateModel()
    {
        $faker = \Faker\Factory::create();

        $library = factory(\App\Models\Library::class)->create();

        $name = $faker->name;
        $id = $library->id;

        $library->name = $name;

        $this->action('PUT', 'Admin\LibraryController@update', [$id], [
                '_token' => csrf_token(),
            ] + $library->toArray());
        $this->assertResponseStatus(302);

        $newLibrary = \App\Models\Library::find($id);
        $this->assertEquals($name, $newLibrary->name);
    }

    public function testDeleteModel()
    {
        $library = factory(\App\Models\Library::class)->create();

        $id = $library->id;

        $this->action('DELETE', 'Admin\LibraryController@destroy', [$id], [
                '_token' => csrf_token(),
            ]);
        $this->assertResponseStatus(302);

        $checkLibrary = \App\Models\Library::find($id);
        $this->assertNull($checkLibrary);
    }

}
