<?php  namespace Tests\Controllers\Admin;

use Tests\TestCase;

class MenuControllerTest extends TestCase
{

    protected $useDatabase = true;

    public function testGetInstance()
    {
        /** @var  \App\Http\Controllers\Admin\MenuController $controller */
        $controller = \App::make(\App\Http\Controllers\Admin\MenuController::class);
        $this->assertNotNull($controller);
    }

    public function setUp()
    {
        parent::setUp();
        $authUser = \App\Models\AdminUser::first();
        $this->be($authUser, 'admins');
    }

    public function testGetList()
    {
        $response = $this->action('GET', 'Admin\MenuController@index');
        $this->assertResponseOk();
    }

    public function testCreateModel()
    {
        $this->action('GET', 'Admin\MenuController@create');
        $this->assertResponseOk();
    }

    public function testStoreModel()
    {
        $menu = factory(\App\Models\Menu::class)->make();
        $this->action('POST', 'Admin\MenuController@store', [
                '_token' => csrf_token(),
            ] + $menu->toArray());
        $this->assertResponseStatus(302);
    }

    public function testEditModel()
    {
        $menu = factory(\App\Models\Menu::class)->create();
        $this->action('GET', 'Admin\MenuController@show', [$menu->id]);
        $this->assertResponseOk();
    }

    public function testUpdateModel()
    {
        $faker = \Faker\Factory::create();

        $menu = factory(\App\Models\Menu::class)->create();

        $name = $faker->name;
        $id = $menu->id;

        $menu->name = $name;

        $this->action('PUT', 'Admin\MenuController@update', [$id], [
                '_token' => csrf_token(),
            ] + $menu->toArray());
        $this->assertResponseStatus(302);

        $newMenu = \App\Models\Menu::find($id);
        $this->assertEquals($name, $newMenu->name);
    }

    public function testDeleteModel()
    {
        $menu = factory(\App\Models\Menu::class)->create();

        $id = $menu->id;

        $this->action('DELETE', 'Admin\MenuController@destroy', [$id], [
                '_token' => csrf_token(),
            ]);
        $this->assertResponseStatus(302);

        $checkMenu = \App\Models\Menu::find($id);
        $this->assertNull($checkMenu);
    }

}
