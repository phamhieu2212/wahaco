<?php  namespace Tests\Controllers\Admin;

use Tests\TestCase;

class PageControllerTest extends TestCase
{

    protected $useDatabase = true;

    public function testGetInstance()
    {
        /** @var  \App\Http\Controllers\Admin\PageController $controller */
        $controller = \App::make(\App\Http\Controllers\Admin\PageController::class);
        $this->assertNotNull($controller);
    }

    public function setUp()
    {
        parent::setUp();
        $authUser = \App\Models\AdminUser::first();
        $this->be($authUser, 'admins');
    }

    public function testGetList()
    {
        $response = $this->action('GET', 'Admin\PageController@index');
        $this->assertResponseOk();
    }

    public function testCreateModel()
    {
        $this->action('GET', 'Admin\PageController@create');
        $this->assertResponseOk();
    }

    public function testStoreModel()
    {
        $page = factory(\App\Models\Page::class)->make();
        $this->action('POST', 'Admin\PageController@store', [
                '_token' => csrf_token(),
            ] + $page->toArray());
        $this->assertResponseStatus(302);
    }

    public function testEditModel()
    {
        $page = factory(\App\Models\Page::class)->create();
        $this->action('GET', 'Admin\PageController@show', [$page->id]);
        $this->assertResponseOk();
    }

    public function testUpdateModel()
    {
        $faker = \Faker\Factory::create();

        $page = factory(\App\Models\Page::class)->create();

        $name = $faker->name;
        $id = $page->id;

        $page->name = $name;

        $this->action('PUT', 'Admin\PageController@update', [$id], [
                '_token' => csrf_token(),
            ] + $page->toArray());
        $this->assertResponseStatus(302);

        $newPage = \App\Models\Page::find($id);
        $this->assertEquals($name, $newPage->name);
    }

    public function testDeleteModel()
    {
        $page = factory(\App\Models\Page::class)->create();

        $id = $page->id;

        $this->action('DELETE', 'Admin\PageController@destroy', [$id], [
                '_token' => csrf_token(),
            ]);
        $this->assertResponseStatus(302);

        $checkPage = \App\Models\Page::find($id);
        $this->assertNull($checkPage);
    }

}
