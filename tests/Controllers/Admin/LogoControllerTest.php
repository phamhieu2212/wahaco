<?php  namespace Tests\Controllers\Admin;

use Tests\TestCase;

class LogoControllerTest extends TestCase
{

    protected $useDatabase = true;

    public function testGetInstance()
    {
        /** @var  \App\Http\Controllers\Admin\LogoController $controller */
        $controller = \App::make(\App\Http\Controllers\Admin\LogoController::class);
        $this->assertNotNull($controller);
    }

    public function setUp()
    {
        parent::setUp();
        $authUser = \App\Models\AdminUser::first();
        $this->be($authUser, 'admins');
    }

    public function testGetList()
    {
        $response = $this->action('GET', 'Admin\LogoController@index');
        $this->assertResponseOk();
    }

    public function testCreateModel()
    {
        $this->action('GET', 'Admin\LogoController@create');
        $this->assertResponseOk();
    }

    public function testStoreModel()
    {
        $logo = factory(\App\Models\Logo::class)->make();
        $this->action('POST', 'Admin\LogoController@store', [
                '_token' => csrf_token(),
            ] + $logo->toArray());
        $this->assertResponseStatus(302);
    }

    public function testEditModel()
    {
        $logo = factory(\App\Models\Logo::class)->create();
        $this->action('GET', 'Admin\LogoController@show', [$logo->id]);
        $this->assertResponseOk();
    }

    public function testUpdateModel()
    {
        $faker = \Faker\Factory::create();

        $logo = factory(\App\Models\Logo::class)->create();

        $name = $faker->name;
        $id = $logo->id;

        $logo->name = $name;

        $this->action('PUT', 'Admin\LogoController@update', [$id], [
                '_token' => csrf_token(),
            ] + $logo->toArray());
        $this->assertResponseStatus(302);

        $newLogo = \App\Models\Logo::find($id);
        $this->assertEquals($name, $newLogo->name);
    }

    public function testDeleteModel()
    {
        $logo = factory(\App\Models\Logo::class)->create();

        $id = $logo->id;

        $this->action('DELETE', 'Admin\LogoController@destroy', [$id], [
                '_token' => csrf_token(),
            ]);
        $this->assertResponseStatus(302);

        $checkLogo = \App\Models\Logo::find($id);
        $this->assertNull($checkLogo);
    }

}
