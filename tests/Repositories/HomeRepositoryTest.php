<?php namespace Tests\Repositories;

use App\Models\Home;
use Tests\TestCase;

class HomeRepositoryTest extends TestCase
{
    protected $useDatabase = true;

    public function testGetInstance()
    {
        /** @var  \App\Repositories\HomeRepositoryInterface $repository */
        $repository = \App::make(\App\Repositories\HomeRepositoryInterface::class);
        $this->assertNotNull($repository);
    }

    public function testGetList()
    {
        $homes = factory(Home::class, 3)->create();
        $homeIds = $homes->pluck('id')->toArray();

        /** @var  \App\Repositories\HomeRepositoryInterface $repository */
        $repository = \App::make(\App\Repositories\HomeRepositoryInterface::class);
        $this->assertNotNull($repository);

        $homesCheck = $repository->get('id', 'asc', 0, 1);
        $this->assertInstanceOf(Home::class, $homesCheck[0]);

        $homesCheck = $repository->getByIds($homeIds);
        $this->assertEquals(3, count($homesCheck));
    }

    public function testFind()
    {
        $homes = factory(Home::class, 3)->create();
        $homeIds = $homes->pluck('id')->toArray();

        /** @var  \App\Repositories\HomeRepositoryInterface $repository */
        $repository = \App::make(\App\Repositories\HomeRepositoryInterface::class);
        $this->assertNotNull($repository);

        $homeCheck = $repository->find($homeIds[0]);
        $this->assertEquals($homeIds[0], $homeCheck->id);
    }

    public function testCreate()
    {
        $homeData = factory(Home::class)->make();

        /** @var  \App\Repositories\HomeRepositoryInterface $repository */
        $repository = \App::make(\App\Repositories\HomeRepositoryInterface::class);
        $this->assertNotNull($repository);

        $homeCheck = $repository->create($homeData->toFillableArray());
        $this->assertNotNull($homeCheck);
    }

    public function testUpdate()
    {
        $homeData = factory(Home::class)->create();

        /** @var  \App\Repositories\HomeRepositoryInterface $repository */
        $repository = \App::make(\App\Repositories\HomeRepositoryInterface::class);
        $this->assertNotNull($repository);

        $homeCheck = $repository->update($homeData, $homeData->toFillableArray());
        $this->assertNotNull($homeCheck);
    }

    public function testDelete()
    {
        $homeData = factory(Home::class)->create();

        /** @var  \App\Repositories\HomeRepositoryInterface $repository */
        $repository = \App::make(\App\Repositories\HomeRepositoryInterface::class);
        $this->assertNotNull($repository);

        $repository->delete($homeData);

        $homeCheck = $repository->find($homeData->id);
        $this->assertNull($homeCheck);
    }

}
