<?php namespace Tests\Repositories;

use App\Models\Post;
use Tests\TestCase;

class PostRepositoryTest extends TestCase
{
    protected $useDatabase = true;

    public function testGetInstance()
    {
        /** @var  \App\Repositories\PostRepositoryInterface $repository */
        $repository = \App::make(\App\Repositories\PostRepositoryInterface::class);
        $this->assertNotNull($repository);
    }

    public function testGetList()
    {
        $posts = factory(Post::class, 3)->create();
        $postIds = $posts->pluck('id')->toArray();

        /** @var  \App\Repositories\PostRepositoryInterface $repository */
        $repository = \App::make(\App\Repositories\PostRepositoryInterface::class);
        $this->assertNotNull($repository);

        $postsCheck = $repository->get('id', 'asc', 0, 1);
        $this->assertInstanceOf(Post::class, $postsCheck[0]);

        $postsCheck = $repository->getByIds($postIds);
        $this->assertEquals(3, count($postsCheck));
    }

    public function testFind()
    {
        $posts = factory(Post::class, 3)->create();
        $postIds = $posts->pluck('id')->toArray();

        /** @var  \App\Repositories\PostRepositoryInterface $repository */
        $repository = \App::make(\App\Repositories\PostRepositoryInterface::class);
        $this->assertNotNull($repository);

        $postCheck = $repository->find($postIds[0]);
        $this->assertEquals($postIds[0], $postCheck->id);
    }

    public function testCreate()
    {
        $postData = factory(Post::class)->make();

        /** @var  \App\Repositories\PostRepositoryInterface $repository */
        $repository = \App::make(\App\Repositories\PostRepositoryInterface::class);
        $this->assertNotNull($repository);

        $postCheck = $repository->create($postData->toFillableArray());
        $this->assertNotNull($postCheck);
    }

    public function testUpdate()
    {
        $postData = factory(Post::class)->create();

        /** @var  \App\Repositories\PostRepositoryInterface $repository */
        $repository = \App::make(\App\Repositories\PostRepositoryInterface::class);
        $this->assertNotNull($repository);

        $postCheck = $repository->update($postData, $postData->toFillableArray());
        $this->assertNotNull($postCheck);
    }

    public function testDelete()
    {
        $postData = factory(Post::class)->create();

        /** @var  \App\Repositories\PostRepositoryInterface $repository */
        $repository = \App::make(\App\Repositories\PostRepositoryInterface::class);
        $this->assertNotNull($repository);

        $repository->delete($postData);

        $postCheck = $repository->find($postData->id);
        $this->assertNull($postCheck);
    }

}
