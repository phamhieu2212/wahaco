<?php namespace Tests\Repositories;

use App\Models\Logo;
use Tests\TestCase;

class LogoRepositoryTest extends TestCase
{
    protected $useDatabase = true;

    public function testGetInstance()
    {
        /** @var  \App\Repositories\LogoRepositoryInterface $repository */
        $repository = \App::make(\App\Repositories\LogoRepositoryInterface::class);
        $this->assertNotNull($repository);
    }

    public function testGetList()
    {
        $logos = factory(Logo::class, 3)->create();
        $logoIds = $logos->pluck('id')->toArray();

        /** @var  \App\Repositories\LogoRepositoryInterface $repository */
        $repository = \App::make(\App\Repositories\LogoRepositoryInterface::class);
        $this->assertNotNull($repository);

        $logosCheck = $repository->get('id', 'asc', 0, 1);
        $this->assertInstanceOf(Logo::class, $logosCheck[0]);

        $logosCheck = $repository->getByIds($logoIds);
        $this->assertEquals(3, count($logosCheck));
    }

    public function testFind()
    {
        $logos = factory(Logo::class, 3)->create();
        $logoIds = $logos->pluck('id')->toArray();

        /** @var  \App\Repositories\LogoRepositoryInterface $repository */
        $repository = \App::make(\App\Repositories\LogoRepositoryInterface::class);
        $this->assertNotNull($repository);

        $logoCheck = $repository->find($logoIds[0]);
        $this->assertEquals($logoIds[0], $logoCheck->id);
    }

    public function testCreate()
    {
        $logoData = factory(Logo::class)->make();

        /** @var  \App\Repositories\LogoRepositoryInterface $repository */
        $repository = \App::make(\App\Repositories\LogoRepositoryInterface::class);
        $this->assertNotNull($repository);

        $logoCheck = $repository->create($logoData->toFillableArray());
        $this->assertNotNull($logoCheck);
    }

    public function testUpdate()
    {
        $logoData = factory(Logo::class)->create();

        /** @var  \App\Repositories\LogoRepositoryInterface $repository */
        $repository = \App::make(\App\Repositories\LogoRepositoryInterface::class);
        $this->assertNotNull($repository);

        $logoCheck = $repository->update($logoData, $logoData->toFillableArray());
        $this->assertNotNull($logoCheck);
    }

    public function testDelete()
    {
        $logoData = factory(Logo::class)->create();

        /** @var  \App\Repositories\LogoRepositoryInterface $repository */
        $repository = \App::make(\App\Repositories\LogoRepositoryInterface::class);
        $this->assertNotNull($repository);

        $repository->delete($logoData);

        $logoCheck = $repository->find($logoData->id);
        $this->assertNull($logoCheck);
    }

}
