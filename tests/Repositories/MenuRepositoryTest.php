<?php namespace Tests\Repositories;

use App\Models\Menu;
use Tests\TestCase;

class MenuRepositoryTest extends TestCase
{
    protected $useDatabase = true;

    public function testGetInstance()
    {
        /** @var  \App\Repositories\MenuRepositoryInterface $repository */
        $repository = \App::make(\App\Repositories\MenuRepositoryInterface::class);
        $this->assertNotNull($repository);
    }

    public function testGetList()
    {
        $menus = factory(Menu::class, 3)->create();
        $menuIds = $menus->pluck('id')->toArray();

        /** @var  \App\Repositories\MenuRepositoryInterface $repository */
        $repository = \App::make(\App\Repositories\MenuRepositoryInterface::class);
        $this->assertNotNull($repository);

        $menusCheck = $repository->get('id', 'asc', 0, 1);
        $this->assertInstanceOf(Menu::class, $menusCheck[0]);

        $menusCheck = $repository->getByIds($menuIds);
        $this->assertEquals(3, count($menusCheck));
    }

    public function testFind()
    {
        $menus = factory(Menu::class, 3)->create();
        $menuIds = $menus->pluck('id')->toArray();

        /** @var  \App\Repositories\MenuRepositoryInterface $repository */
        $repository = \App::make(\App\Repositories\MenuRepositoryInterface::class);
        $this->assertNotNull($repository);

        $menuCheck = $repository->find($menuIds[0]);
        $this->assertEquals($menuIds[0], $menuCheck->id);
    }

    public function testCreate()
    {
        $menuData = factory(Menu::class)->make();

        /** @var  \App\Repositories\MenuRepositoryInterface $repository */
        $repository = \App::make(\App\Repositories\MenuRepositoryInterface::class);
        $this->assertNotNull($repository);

        $menuCheck = $repository->create($menuData->toFillableArray());
        $this->assertNotNull($menuCheck);
    }

    public function testUpdate()
    {
        $menuData = factory(Menu::class)->create();

        /** @var  \App\Repositories\MenuRepositoryInterface $repository */
        $repository = \App::make(\App\Repositories\MenuRepositoryInterface::class);
        $this->assertNotNull($repository);

        $menuCheck = $repository->update($menuData, $menuData->toFillableArray());
        $this->assertNotNull($menuCheck);
    }

    public function testDelete()
    {
        $menuData = factory(Menu::class)->create();

        /** @var  \App\Repositories\MenuRepositoryInterface $repository */
        $repository = \App::make(\App\Repositories\MenuRepositoryInterface::class);
        $this->assertNotNull($repository);

        $repository->delete($menuData);

        $menuCheck = $repository->find($menuData->id);
        $this->assertNull($menuCheck);
    }

}
