<?php namespace Tests\Repositories;

use App\Models\Library;
use Tests\TestCase;

class LibraryRepositoryTest extends TestCase
{
    protected $useDatabase = true;

    public function testGetInstance()
    {
        /** @var  \App\Repositories\LibraryRepositoryInterface $repository */
        $repository = \App::make(\App\Repositories\LibraryRepositoryInterface::class);
        $this->assertNotNull($repository);
    }

    public function testGetList()
    {
        $libraries = factory(Library::class, 3)->create();
        $libraryIds = $libraries->pluck('id')->toArray();

        /** @var  \App\Repositories\LibraryRepositoryInterface $repository */
        $repository = \App::make(\App\Repositories\LibraryRepositoryInterface::class);
        $this->assertNotNull($repository);

        $librariesCheck = $repository->get('id', 'asc', 0, 1);
        $this->assertInstanceOf(Library::class, $librariesCheck[0]);

        $librariesCheck = $repository->getByIds($libraryIds);
        $this->assertEquals(3, count($librariesCheck));
    }

    public function testFind()
    {
        $libraries = factory(Library::class, 3)->create();
        $libraryIds = $libraries->pluck('id')->toArray();

        /** @var  \App\Repositories\LibraryRepositoryInterface $repository */
        $repository = \App::make(\App\Repositories\LibraryRepositoryInterface::class);
        $this->assertNotNull($repository);

        $libraryCheck = $repository->find($libraryIds[0]);
        $this->assertEquals($libraryIds[0], $libraryCheck->id);
    }

    public function testCreate()
    {
        $libraryData = factory(Library::class)->make();

        /** @var  \App\Repositories\LibraryRepositoryInterface $repository */
        $repository = \App::make(\App\Repositories\LibraryRepositoryInterface::class);
        $this->assertNotNull($repository);

        $libraryCheck = $repository->create($libraryData->toFillableArray());
        $this->assertNotNull($libraryCheck);
    }

    public function testUpdate()
    {
        $libraryData = factory(Library::class)->create();

        /** @var  \App\Repositories\LibraryRepositoryInterface $repository */
        $repository = \App::make(\App\Repositories\LibraryRepositoryInterface::class);
        $this->assertNotNull($repository);

        $libraryCheck = $repository->update($libraryData, $libraryData->toFillableArray());
        $this->assertNotNull($libraryCheck);
    }

    public function testDelete()
    {
        $libraryData = factory(Library::class)->create();

        /** @var  \App\Repositories\LibraryRepositoryInterface $repository */
        $repository = \App::make(\App\Repositories\LibraryRepositoryInterface::class);
        $this->assertNotNull($repository);

        $repository->delete($libraryData);

        $libraryCheck = $repository->find($libraryData->id);
        $this->assertNull($libraryCheck);
    }

}
