

// for Froala editor
$("#froala-editor").froalaEditor({
    toolbarInline: false,
    pastePlain: true,
    heightMin: 200,
    heightMax: 500,
    imageUploadURL: Boilerplate.imageUploadUrl,
    imageUploadParams: Boilerplate.imageUploadParams,
    imageManagerLoadURL: Boilerplate.imagesLoadURL,
    imageManagerLoadParams: Boilerplate.imagesLoadParams,
    imageManagerDeleteURL: Boilerplate.imageDeleteURL,
    imageManagerDeleteParams: Boilerplate.imageDeleteParams,
    imageManagerDeleteMethod: "DELETE"
}).on('froalaEditor.initialized', function (e, editor) {

}).on('froalaEditor.focus', function (e, editor) {

}).on('froalaEditor.image.inserted', function (e, editor, img) {

}).on('froalaEditor.image.error', function (e, editor, error) {

}).on('froalaEditor.image.removed', function (e, editor, $img) {
    Boilerplate.imageDeleteParams.src = $img.attr('src');

    $.ajax({
        url: Boilerplate.imageDeleteURL,
        method: 'DELETE',
        data: Boilerplate.imageDeleteParams,
        error: function (xhr, error) {
            console.log(error);
        },
        success: function (response) {
            console.log(response);
        }
    });
});
